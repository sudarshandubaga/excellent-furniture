<?php
namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\WishlistModel as Wishlist;
use App\Query;
use Illuminate\Support\Facades\Mail;
use DB;

class Ajax extends BaseController {
    public function index( Request $request, $action = NULL ) {
        $post  = $request->input();

        $param = compact('post');
        $re = call_user_func_array(array($this, $action), $param);

        return response()->json($re);
    }

    public function change_your_order_layout( $post = [] ) {
        $country_code = $post['country'];
        $html         = \View::make('frontend/template/checkout_your_order', ['post' => $post, 'country_code' => $country_code]);

        $re = [
            'status'    => TRUE,
            'html'      => html_entity_decode( $html )
        ];

        return $re;
    }

    public function push_shuffle( $post = [] ) {

        $orders = DB::table('orders')->where('order_is_paid', 'Y')->orderBy('order_id', 'DESC')->select('order_id')->take(10)->get();
        $oids = [];
        foreach($orders as $o) {
            $oids[] = $o->order_id;
        }

        if(!empty($oids)) {
            $oid = $oids[ rand(0, count($oids)) ];
            $order = DB::table('orders AS o')
                        ->join('order_products AS op', 'o.order_id', 'op.opro_oid')
                        ->join('products AS p', 'op.opro_pid', 'p.product_id')
                        ->where('order_id', $oid)
                    ->first();

            $billing = @unserialize( html_entity_decode( $order->order_billing ) );

            $message = "
                <div class='row'>
                    <div class='col-4'>
                        <img src='".url('imgs/product/'.$order->product_image)."' />
                    </div>
                    <div class='col-8'>{$billing['name']} from {$billing['country']} just purchased <strong>{$order->product_name}</strong></div>
                </div>
            ";

            $re = [
                'status'    => TRUE,
                'message'   => $message
            ];
        } else {
            $re = [
                'status' => FALSE
            ];
        }

        return $re;
    }

    public function notify_me( $post = [] ) {
        $email = $post['email'];

        $to          = "orders@chitrani.com";
        $from        = $email;
        $sender      = 'Customer';
        $subject     = "Notify Me Enquiry For Out Of Stock Product - Chitrani";

        $fields   = [
            'subject'    => $subject,
            'post'       => $post
        ];

        Mail::send(['html' => 'email.notify_me'], $fields, function($message) use ($to, $from, $subject, $sender) {
             $message->to($to, 'Chitrani')->subject($subject);
             $message->from($from, $sender);
        });

        $re = [
            'status'    => TRUE
        ];

        return $re;
    }

    public function apply_coupon( $post = [] ) {
        $country_code = $post['country'];
        $coupon_code  = $post['coupon_code'];

        $check        = DB::table('coupons')->where('coupon_code', $coupon_code)->first();

        if(!empty($check->coupon_id)) :

            $carts    = session('cart_session');
            if(!empty($carts)) {
                $exl_pros = explode(",", $check->coupon_exclude_products);
                foreach($carts as $pid => $data) {
                    if(in_array($pid, $exl_pros)) {
                        unset($carts[$pid]);
                    }
                }
            }
            $pids     = array_keys($carts);

            $flag     = FALSE;

            if(!empty($check->coupon_include_products)) {
                $inc_pros = explode(",", $check->coupon_include_products);
                if(!empty($carts)) {
                    foreach($carts as $pid => $data) {
                        if(in_array($pid, $inc_pros)) {
                            $flag = TRUE;
                        }
                    }
                }
            } else {
                $flag = TRUE;
            }

            if($flag) {

                session(['coupon_code' => $coupon_code]);

                $html         = \View::make('frontend/template/checkout_your_order', ['post' => $post, 'country_code' => $country_code]);

                $re = [
                    'status'    => TRUE,
                    'html'      => html_entity_decode( $html )
                ];

            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'Coupon code is not valid'
                ];
            }

        else :
            $re = [
                'status'    => FALSE,
                'message'   => 'Coupon code is not valid'
            ];
        endif;

        return $re;
    }

    public function remove_coupon( $post = [] ) {
        session()->forget('coupon_code');

        $country_code = $post['country'];
        $html         = \View::make('frontend/template/checkout_your_order', ['post' => $post, 'country_code' => $country_code]);

        $re = [
            'status'    => TRUE,
            'html'      => html_entity_decode( $html )
        ];

        return $re;
    }

    public function search_products( $post = [] ) {
        $info 			= Query::ip_info();
        $country_code 	= !empty($info['country_code']) ? $info['country_code'] : 'IN';

        $html = \View::make('frontend/inc/product_grid', compact('post', 'country_code'));

        $url_sufix = @$post['category'];
        if(!empty($post['subcategory'])) {
            $s           = $post['subcategory'];
            $url_sufix  .= '?category='.urlencode($s);
        }

        if(!empty($post['sort_by'])) {
            $sort_by     = $post['sort_by'];
            $url_sufix  .= $url_sufix != @$post['category'] ? '&sort_by='.urlencode($sort_by) : '?sort_by='.urlencode($sort_by);
        }

        $re = array(
            'status'    => TRUE,
            'html'      => html_entity_decode( $html ),
            'url_suffix' => $url_sufix,
            'post'      => $post,
        );

        return $re;
    }

    public function user_login ( Request $request ) {

        $post = $request->input('record');
        $is_exists = DB::table('users')
            ->select( DB::raw('COUNT(*) AS total, user_id, user_password') )
            ->where('user_login', $post['user_login'])
            ->where('user_is_deleted', 'N')
            ->first();

        if($is_exists->total == 1) {
            if(password_verify($post['user_password'], $is_exists->user_password)) {
                session(['user_auth' => $is_exists->user_id]);

                $re = array(
                    'status'    => TRUE,
                    'message'   => 'Login success! Redirecting, please wait...'
                );
            } else {
                $re = array(
                    'status'    => FALSE,
                    'message'   => 'Login failed! Passowrd is not matched.'
                );
            }
        } else {
            $re = array(
                'status'    => FALSE,
                'message'   => 'Login failed! Username doesn\'t exists.'
            );
        }

        header('Content-Type: application/json');
        print json_encode($re, JSON_PRETTY_PRINT);
    }


    public function remove_to_cart(Request $request) {
        $post = $request->input();
        extract( $post );

        $cart_session = array();
        if(session()->has('cart_session')) {
            $cart_session = session('cart_session');
        }

        unset($cart_session[$id]);

        session(['cart_session' => $cart_session]);

        $product_info = DB::table('products')->where('product_id', $id)->first();

        $re = array(
            'status'    => true,
            'message'   => $product_info->product_name.' removed from cart.',
            'counter'   => count($cart_session)
        );

        header('Content-Type: application/json');
        print json_encode($re, JSON_PRETTY_PRINT);
    }

    public function add_to_cart(Request $request) {

        $post = $request->input();
        extract($post); // array to variable

        $cart_session       = session('cart_session');
        $cart_session[$id]  = !empty($cart_session[$id]) ? $cart_session[$id] + $qty : $qty;

        session(['cart_session' => $cart_session]);

        $info 			= Query::ip_info();
    	$country_code 	= !empty($info['country_code']) ? $info['country_code'] : 'IN';

        $product = DB::table('products')->where('product_id', $id)->first();

        $top_cart = \View::make('frontend/template/top_header_cart', ['cart_session' => $cart_session, 'country_code' => $country_code]);

        $arr = array(
            'message'   => $product->product_name." added to cart.",
            'top_cart'  => html_entity_decode( $top_cart ),
            'counter'   => count($cart_session)
        );

        return response()->json( $arr );
    }

    // Update Cart
    public function update( Request $request ) {
        $post = $request->input();
        extract( $post );

        $info 			= Query::ip_info();
    	$country_code 	= !empty($info['country_code']) ? $info['country_code'] : 'IN';

        $cart_session = array();
        if(session()->has('cart_session')) {
            $cart_session = session('cart_session');
        }
        $cart_session[$pid] = $qty;

        session(['cart_session' => $cart_session]);

        $product_info   = DB::table('products')->where('product_id', $pid)->first();

        $cartArr = [];
        foreach($cart_session as $pid => $qty) {
            $product = DB::table('products')->where('product_id', $pid)->first();
            $product->qty = $qty;
            $cartArr[] = $product;
        }

        $total = 0;
        foreach($cartArr as $cart) :
            if($country_code == 'IN') {
                $total += $cart->product_sell_price * $cart->qty;
            } else {
                $total += $cart->product_sell_price_dollar * $cart->qty;
            }
        endforeach;

        $cart_grid      = \View::make('frontend/inc/cart_product', ['cart_session' => $cart_session, 'country_code' => $country_code, 'cartArr' => $cartArr, 'total' => $total]);

        $re = array(
            'status'    => true,
            'message'   => $product_info->product_name.' updated in cart.',
            'counter'   => count($cart_session),
            'cart_grid' => html_entity_decode( $cart_grid )
        );

        return response()->json($re);
        // header('Content-Type: application/json');
        // print json_encode($re, JSON_PRETTY_PRINT);
    }

    public function wishlist(Request $request) {
        $user_id = session('user_auth');
        $post = $request->input();
        $id = $post['id'];
        $arr = array(
            "wish_uid" => $user_id,
            "wish_pid" => $id
        );


        $records = wishlist::where('wish_uid', $user_id)->where('wish_pid', $id)->first();

        $product = DB::table('products')->where('product_id', $id)->first();

        if (empty($records))
        {
            wishlist::insert($arr);
            $arr = array(
                'message' => $product->product_name." Added to wishlist."
            );
        }else{
            wishlist::where('wish_uid', $user_id)->where('wish_pid', $id)->delete();
            $arr = array(
                'message' => $product->product_name." Removed from wishlist"
            );
        }

        header('Content-Type: application/json');
        print json_encode($arr, JSON_PRETTY_PRINT); // convert array to json
    }


public function like(Request $request) {
    $user_id = session('user_auth');
    $post    = $request->input();
    $rid     = $post['rid'];
    $like    = $post['like'];
    $dislike = $post['dislike'];
    $arr     = array(
                 "like_uid"     => $user_id,
                 "like_rid"     => $rid,
                 "like_like"    => $like,
                 "like_dislike" => $dislike
              );


    $records = DB::table('likes')->where('like_uid', $user_id)->where('like_rid', $rid)->first();

    if (empty($records))
    {
       DB::table('likes')->insert($arr);
       $likes = DB::table('likes')
               ->where('like_rid', $rid)
               ->select(DB::raw('SUM(`like_like`) AS lk_total, SUM(`like_dislike`) AS dlk_total'))
               ->first();
       $arr = array(
           'like' => $likes->lk_total,
            'dislike' => $likes->dlk_total
       );
    }

    header('Content-Type: application/json');
    print json_encode($arr, JSON_PRETTY_PRINT); // convert array to json
}
}
