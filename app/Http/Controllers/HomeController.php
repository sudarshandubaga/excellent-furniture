<?php

namespace App\Http\Controllers;

use App\Http\Controllers\admin\Product;
use Illuminate\Routing\Controller as BaseController;
use App\Model\Slider;
use App\Model\Category;
use App\Model\ProductModel as Prod;
use App\Model\BlogModel as Blog;
use Illuminate\Http\Request;
use DB;

class HomeController extends BaseController {
   public function index() {
    	$title 	       = "Chitrani | Kids Wooden Toys & Furniture Online Store";
    	$page 	       = "home";

    	$sliders       = Slider::where('slider_is_deleted', 'N')->get();
    	$categories    = Category::join('categories AS mc', 'categories.category_parent', 'mc.category_id')
                            ->where('categories.category_parent', 3)
                            ->where('categories.category_is_deleted', 'N')
                            ->select('mc.category_slug AS mslug', 'categories.*')
                            ->get();

        foreach($categories as $k => $c) {
            $product_sum                    = Prod::where('product_is_deleted', 'N')->where('product_subcategory', $c->category_id)->count();
            $categories[$k]->product_count  = $product_sum;
        }

        $products      = Prod::where('product_is_home','Y' )->where('product_is_deleted','N')->get();

	    $blogs         = Blog::where('post_is_deleted','N')->paginate(3);
    	$data 	       = compact('page', 'title', 'sliders', 'categories' , 'products', 'blogs');
    	return view('frontend/layout', $data);
    }
}
