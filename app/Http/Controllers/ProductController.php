<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\ProductModel as Prod;
use App\Model\WishlistModel as wishlist;
use DB;

class ProductController extends BaseController {
    public function index(Request $request, $mcategory = null, $scategory = null) {
        $mcat_name = 'Search';
        if(empty($mcategory)) $mcategory = "toys";
        
        if(!empty($mcategory)) {
            $category   = Category::where('category_slug', $mcategory)->first();
            $mcat_name  = $category->category_name;

            $subcategories = Category::join('categories AS mcat', 'categories.category_parent', '=',  'mcat.category_id')
                ->where('mcat.category_slug', $mcategory)
                ->where('categories.category_is_deleted', 'N')
                ->select('categories.*')
                ->get();
        }

        $post       = $request->input();

        // print_r($post);
        if(!empty($post['category'])) {
            $post['subcategory'] = $post['category'];
            $post['category']    = $mcategory;
        }

        $mcat_name = ucwords( strtolower($mcat_name) );

        $title 	= "{$mcat_name} | Chitrani";

        $colors = DB::table('colors')->where('color_is_deleted', 'N')->get();

        $page   = "products";
        $data   = compact('page', 'title', 'subcategories', 'mcategory', 'scategory', 'colors', 'mcat_name', 'post');
        return view('frontend/layout', $data);
    }
}
