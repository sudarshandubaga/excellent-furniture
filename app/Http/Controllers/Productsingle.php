<?php

namespace App\Http\Controllers;

use App\Http\Controllers\admin\Product;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Model\ProductModel as Prod;
use App\Query;
use DB;

class Productsingle extends BaseController
{
    public function index( Request $request, $slug, $clear = NULL) {
        $record = Prod::join('categories as mcat', 'mcat.category_id', '=', 'products.product_category')
                    ->leftjoin('categories as scat', 'scat.category_id', '=', 'products.product_subcategory')
                    ->where('product_slug', $slug)->where('product_is_deleted', 'N')
                    ->select('products.*','mcat.category_name AS mcategory', 'scat.category_name AS scategory', 'mcat.category_slug AS mslug', 'scat.category_slug AS sslug')
                    ->first();

        $title       = $record->product_meta_title;
        $meta        = [
            'title'         => $record->product_meta_title,
            'keywords'      => $record->product_meta_keywords,
            'description'   => $record->product_meta_description
        ];
        $page         = "product-single";

        $session_auth = session('user_auth');

        // print_r($input);
        if ($request->isMethod('post')) {
             $input		 = $request->input('rating');
             DB::table('reviews')->insert($input);

             $user_info  = Query::profile_info();

             $to          = "info@chitrani.com";
             $from        = $user_info->user_email;
             $sender      = $user_info->user_name;
             $subject     = $user_info->user_name." placed an review - Chitrani";

             $fields   = [
                 'subject'    => $subject,
                 'input'      => $user_info
             ];

             Mail::send(['html' => 'email.review'], $fields, function($message) use ($to, $from, $subject, $sender) {
                  $message->from($from, $sender);
                  $message->to($to, 'Chitrani')
                         ->subject($subject)
                         ->replyTo($from, $sender);
             });

             return redirect()->back()->with('success', 'Your review under verification and will be shown soon.');
        }
        $user = session('user_auth');
        // $cart = session('cart_session');
        $gall = DB::table('product_images')->where('pimage_pid', $record->product_id)->get();
        $related = prod::where('product_subcategory', $record->product_subcategory)
                       ->where('product_id', '!=' , $record->product_id)
                       ->get();
        $reviews = DB::table('reviews')
                      ->leftjoin('users as us', 'us.user_id', '=', 'reviews.review_uid')
                      ->orderBy('reviews.review_id', 'DESC')
                      ->where('review_is_active', 'Y')
                      ->where('reviews.review_pid', $record->product_id)
                      ->paginate(10);

        $avg_reviews = DB::table('reviews')->where('reviews.review_pid', $record->product_id)->where('review_is_active', 'Y')->avg('review_rating');
        $max_reviews = DB::table('reviews')->where('reviews.review_pid', $record->product_id)->where('review_is_active', 'Y')->max('review_rating');

        // print_r($reviews);
        $wishcount = DB::table('wishlist')->where('wish_pid', $record->product_id)->count();
        $user_ip = $this->getUserIP();
        $is_exists = DB::table('ip_products')->where('ipp_pid', $record->product_id)->where('ipp_ip', $user_ip)->count();
        $ip_arr = [
            'ipp_pid' => $record->product_id,
            'ipp_ip'  => $user_ip,
            'ipp_created_on' => date('Y-m-d H:i:s', time())
        ];
        if($is_exists) {
            DB::table('ip_products')->where('ipp_pid', $record->product_id)->where('ipp_ip', $user_ip)->update($ip_arr);
        } else {
            DB::table('ip_products')->insert($ip_arr);
        }

        if($clear == 'clear') {
            DB::table('ip_products')->where('ipp_ip', $user_ip)->delete();
            return redirect('product/'.$slug);
        }

        $recents = DB::table('products AS p')
                        ->join('ip_products AS ipp', 'p.product_id', 'ipp.ipp_pid')
                        ->where('ipp_ip', $user_ip)
                        ->orderBy('ipp_created_on', 'desc')
                        ->paginate(50);

        $data = compact('page', 'title' ,'record', 'related', 'user', 'gall', 'reviews', 'wishcount', 'recents', 'clear', 'slug', 'meta', 'avg_reviews', 'max_reviews');
        return view('frontend/layout', $data);
    }

    public function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                  $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }
}
