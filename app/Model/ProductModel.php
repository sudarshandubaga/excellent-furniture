<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model {
    public      $timestamps     = false;
    protected   $table 		    = "products";
    protected   $primaryKey 	= "product_id";
}
