const baseurl = $('#base_url').val();
function push_shuffle() {
    $.ajax({
        url: baseurl+'/ajax/push_shuffle',
        type: 'POST',
        data: {
            'shuffle': 1
        },
        success: function(res) {
            if(res.status) {
                $('.push-notification').find('.pn-message').html(res.message);
                $('.push-notification').addClass('active');
                setTimeout(function() {
                    $('.push-notification').removeClass('active');
                }, 6000);
            }
        }
    });
}
$(document).ready(function () {
    // Ajax Request
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.refresh-captcha').on('click', function() {
        $('.captcha-image').attr('src', baseurl+'/captcha-code/?' + Date.now());
    });

    $('.nav-icon').on('click', function() {
        $('body').toggleClass('open_nav');
    });
    $(document).on('click', '.filter-icon', function(e) {
        e.preventDefault();
        $('body').toggleClass('open_filter');
    });

    $('.remove-coupon-strip').on('click', function() {
        $(this).closest('.coupon_strip').remove();
    });

    push_shuffle();
    setInterval(function() {
        push_shuffle();
    }, 25000);

    $('.push-notification .close').on('click', function(e) {
        e.preventDefault();

        $('.push-notification').removeClass('active');
    });

    $('#notifyForm').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                form.find('.form-control').val('');
                form.find('.row').after( '<p class="text-success">Thank you for your interest, You\'ll be notified once in stock.</p>' );
            }
        });
    });

    $(document).on('click', '.coupon_apply_btn', function() {
        var coupon_code = $('#order_coupon_code').val();
        var country     = $('#shipping_country').val();
        $('.coupon_error').remove();

        $.ajax({
            url: baseurl+'/ajax/apply_coupon/',
            type: 'POST',
            data: {
                'coupon_code': coupon_code,
                'country': country
            },
            success: function(res) {
                if(res.status)
                    $('#checkout_your_order').html(res.html);
                else
                    $('#order_coupon_code').closest('.form-group').append( '<p class="text-danger coupon_error">Coupon Code is not valid.</p>' );
            }
        });
    });

    $(document).on('click', 'a[href="#remove_coupon_code"]', function(e) {
        e.preventDefault();

        var country     = $('#shipping_country').val();

        $.ajax({
            url: baseurl+'/ajax/remove_coupon/',
            type: 'POST',
            data: {
                'country': country
            },
            success: function(res) {
                $('#checkout_your_order').html(res.html);
            }
        });
    });

    $('#checkoutForm').on('submit', function(e) {
        if(!$('#checkAgree').prop('checked')) {
            e.preventDefault();

            swal({
                  title: "Warning",
                  text: 'Please check the Terms and Conditions',
                  icon: "warning"
            });
        }
    });

    // $('.filter-icon').on('click', function() {
    //     $('body').toggleClass('open_filter');
    // });

    const symbol  = localStorage.getItem("symbol");
    const c_value = localStorage.getItem("c_value");

    if(symbol != '' && c_value != '' && symbol != undefined && c_value != undefined) {
        $('.product-price').each(function() {
            var usd     = $(this).data('usd');
            var inr     = $(this).data('inr');
            var amount  = symbol == "₹" ? inr : Math.ceil(usd * c_value);
            console.log(symbol + c_value);
            $(this).html( symbol+' '+amount );
        });
    }

    // Currency Converter
    $(document).on('click', '.currency_switcher li a', function(e) {
        e.preventDefault();
        var symbol = $(this).data('symbol');
        var value  = $(this).data('value');

        $('.product-price').each(function() {
            var usd     = $(this).data('usd');
            var inr     = $(this).data('inr');
            var amount  = symbol == "₹" ? inr : Math.ceil(usd * value);

            $(this).html( symbol+' '+amount );
        });

        localStorage.setItem("symbol", symbol);
        localStorage.setItem("c_value", value);
        // $.ajax({
        //     url: baseurl+'/ajax/currency_switcher',
        //     type: 'POST',
        //     data: {
        //         symbol: symbol,
        //         value: value
        //     },
        //     success: function(res) {
        //         console.log('currency switcher applied');
        //     }
        // });
    });

    // Upload Image
    $(".file-upload input[type=file]").change(function() {
        var target = $(this).closest(".file-upload").find("img");
        readURL(this, target);
    });

    $(".clear_recent").on('click', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).data('url'),
                type: 'POST',
            });
            location.reload();
    });

    $('#billing_same_as_shipping').on('click', function() {
        $.each($("[name^='record[order_shipping]']"), function( key, value ) {
            var name = $(this).attr('name').replace('record[order_shipping]','');

            $("[name='record[order_billing]"+name+"']").val( $(this).val() ).trigger('change');
        });
    });

    $('#shipping_country').on('change', function() {
        var con = $(this).val();

        if(con != '') {
            $.ajax({
                url: baseurl+'/ajax/change_your_order_layout/',
                type: 'POST',
                data: {
                    'country': con
                },
                success: function(res) {
                    $('#checkout_your_order').html(res.html);
                }
            });
        }
    });

    $("a.lightbox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	200,
		'overlayShow'	:	false
	});

    // Update Cart
    $(document).on('click', '.update_cart_value', function(e) {
        e.preventDefault();

        var inp = $(this).closest('.increment').find('.qty_input');

        var action  = inp.data('action'),
            pid     = inp.data('pid'),
            qty     = inp.val();
    });

    $('#cartProducts').on('change', '.qty_input', function(e) {
        e.preventDefault();

        var inp     = $(this);

        var action  = inp.data('action'),
            pid     = inp.data('pid'),
            qty     = inp.val();

        $('#loadingBox').show();
        $.ajax({
            url: action,
            type: 'POST',
            data: {
                'pid': pid,
                'qty': qty
            },
            success: function(res) {
                $('.cart_counter').text( res.counter );

                $('#loadingBox').hide();
                console.log( res.cart_grid );
                $('#cartProducts').html( res.cart_grid );

                // location.reload();

            }
        });
    });

    $(document).on('submit', '#sortingForm', function(e) {
        e.preventDefault();
        var action = $(this).attr('action');
        // var mcat   = $('#sorting_category').val();
        $('.product_loading').show();
        $.ajax({
            url: action,
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                $('.product_loading').hide();

                $("#productGrid").html( res.html );

                $('body').removeClass('open_filter');

                window.history.pushState(baseurl, '', baseurl+'/'+res.url_suffix);
            }
        });
    });

    $(document).on('click', 'a[rel="product-filter"]', function(e) {
        e.preventDefault();
        var mcat = $(this).data('category');
        var scat = $(this).data('subcategory');

        $('#sorting_category').val(mcat);
        $('#sorting_subcategory').val(scat);

        $(this).closest('form').trigger('submit');
    });

    $(document).on('click', '.header_search_icon', function(e) {
        e.preventDefault();

        $('.search-body').fadeIn();
    });

    $(document).on('click', '.search-close', function(e) {
        e.preventDefault();

        $('.search-body').fadeOut();
    });

    $(document).on('change', '.sort_by_dropdown', function() {

        $(this).closest('form').trigger('submit');

    });

    // Login User
    $("#loginForm").on("submit", function(e) {
        e.preventDefault();
        var form            = $(this);
        var is_valid        = form.is_valid();
        var fmsg            = form.find('.form-msg');
        var action          = form.attr('action');
        var redirect_url    = form.data('redirect');

        if(is_valid) {
            fmsg.addClass('alert alert-info').removeClass('alert-danger alert-success').html('Progressing, please wait...');

            $.ajax({
                url: action,
                type: 'POST',
                data: form.serialize(),
                success: function(res) {
                    res = JSON.parse(res);
                    console.log(res);
                    if(res.status) {
                        fmsg.removeClass('alert-info').addClass('alert-success').html(res.message);

                        if(redirect_url == '')
                            location.reload();
                        else
                            window.location = redirect_url;

                    } else {
                        fmsg.removeClass('alert-info').addClass('alert-danger').html(res.message);
                    }
                }
            });
        }
    });

    // Add To Cart
    $(document).on('click', '.add_to_cart_product', function() {
        var qty = $('#productQty').val();

        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id': $(this).data('id'),
                'qty': qty
            },
            success: function(result) {

                $('.cart_counter').text(result.counter).addClass('active');

                $('.minicart').html(result.top_cart);

                swal({
                      title: "Success",
                      text: result.message,
                      icon: "success"
                });
            }
        });
    });
    $('.buy-nowbtn').on('click', function() {
        var qty = $('#productQty').val();

        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id' : $(this).data('id'),
                'qty': qty
            },
            success: function(result) {

                $('.cart_counter').text(result.counter).addClass('active');

                window.location = baseurl+"/checkout";
            }
        });
    });
    $(document).on('click', '.add_to_cart', function() {
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id': $(this).data('id'),
                'qty': 1
            },
            success: function(result) {

                $('.cart_counter').text(result.counter).addClass('active');

                $('.minicart').html(result.top_cart);

                swal({
                      title: "Success",
                      text: result.message,
                      icon: "success"
                });
            }
        });
    });
    $(document).on('click', '.buy_now_btn', function() {
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id': $(this).data('id'),
                'qty': 1
            },
            success: function(result) {

                $('.cart_counter').text(result.counter).addClass('active');

                window.location = baseurl+"/checkout";
            }
        });
    });
    $(document).on('click', '.remove_item_from_cart', function() {
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id': $(this).data('id')
            },
            success: function(result) {
                result = JSON.parse(result);
                // alert( result.message );
                $('.cart_counter').text(result.counter);
                swal({
                  title: "Success",
                  text: result.message,
                  icon: "success"
                })
                location.reload();
            }
        });
    });

    // Side product search
    $(document).on('click', '.ajax_action', function() {
        $(this).closest('form').trigger('submit');
    });

    $('form#searchProductForm').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function(res) {
                $("#productGrid").html(res.html);
            }
        });
    });

    $(document).on('click', '.radius-text', function(e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $(target).trigger('click');
    });
    // Side product search end



    //wishlist
    $('.wishlist').on('click', function() {
        $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'id': $(this).data('id')
            },
            success: function(result) {
                result = JSON.parse(result);

                swal({
                    title:result.message,
                    icon: "success"   //icon:"warning" icon:"info" icon:"error"
                });
                location.reload();
            }
        });
    });

    //like
    $('.like').on('click', function() {
        var box = $(this).closest('.text-right');
       $.ajax({
            url: $(this).data('url'),
            type: 'POST',
            data: {
                'rid': $(this).data('rid'),
                 'like': $(this).data('like'),
                 'dislike': $(this).data('dislike')
            },
            success: function(result) {
                result = JSON.parse(result);
                $(box).children('.vallike').text(result.like);
                $(box).children('.valdislike').text(result.dislike);
            }
       });
    });

    // Checkout Accordion JS Start

    $('.name-id').first().slideDown();
    $('.block-down').first().next().find('.name-id').slideDown();

    // Checkout Accordion JS End

    $(document).on("click", ".odd.plus", function() {
        var parent  = $(this).closest("div.increment"),
            input   = parent.find(".qty_input"),
            current = input.val();

        var new_val = parseInt( current ) + 1;

        input.val( new_val ).trigger('change');
    });

    $(document).on("click", ".add.minus", function() {
        var parent  = $(this).closest("div.increment"),
            input   = parent.find(".qty_input"),
            current = input.val();

        var new_val = current > 1 ? parseInt( current ) - 1 : 1;

        input.val( new_val ).trigger('change');
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('#return').fadeIn();
        } else {
            $('#return').fadeOut();
        }
    });

    $('.owl-carousel.owl-carousel3').owlCarousel({
        margin: 5,
        items: 8,
        loop: false,
        autoplay: false,
        nav: false,
        // responsive:{
        //     0:{
        //         items: 1,
        //         loop: true,
        //         nav: true
        //     },
        //     600:{
        //         items: 3,
        //         nav: false
        //     },
        //     1000:{
        //         items: 3,
        //         loop: false,
        //         nav: false,
        //         mouseDrag: false
        //     }
        // }
    });

    $('.owl-carousel.owl-carousel2').owlCarousel({
        margin: 20,
        responsive:{
            0:{
                items: 1,
                loop: true,
                nav: true
            },
            600:{
                items: 3,
                loop: false,
                nav: false
            },
            1000:{
                items: 3,
                loop: false,
                nav: false,
                mouseDrag: false
            }
        }
    });

    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        responsive:{
            0: {
                items: 2
            },
            600:{
                items: 3
            },
            1000:{
                items: 4
            }
        }
    });

    $('#return').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $(".heart>i").on("click",function(){
        $(this).toggleClass("icon-heart icon-heart-o");
    });

    // range slider

    // $("#ex12a").slider({ id: "slider12a", min: 0, max: 10, value: 5 });
    // $("#ex12b").slider({ id: "slider12b", min: 0, max: 10, range: true, value: [3, 7] });
    // $("#ex12c").slider({ id: "slider12c", min: 0, max: 10, range: true, value: [3, 7] });
    //
    // var slider = $("#product_price_range").slider({
    //     tooltip: 'always',
    //     tooltip_position:'top'
    // });
    // slider.on('slide', function(range) {
    //     console.log(range);
    // });
    // slider.on('slideStop', function(range) {
    //     // console.log(range);
    //     $('#productSearchForm').trigger('submit');
    // });

});

$(function() {
        $(".next-btn").on('click', function(e) {
            e.preventDefault();

            var parent = $(this).closest('.checkout-step');

            parent.find('.step-body').slideUp();
            parent.next('.checkout-step').find('.step-body').slideDown();
        });
        $(".prev-btn").on('click', function(e) {
            e.preventDefault();

            var parent = $(this).closest('.checkout-step');

            parent.find('.step-body').slideUp();
            parent.prev('.checkout-step').find('.step-body').slideDown();
        });


    });





// Range slider

// var sliderA = new Slider("#ex12a", { id: "slider12a", min: 0, max: 10, value: 5 });
// var sliderB = new Slider("#ex12b", { id: "slider12b", min: 0, max: 10, range: true, value: [3, 7] });
// var sliderC = new Slider("#ex12c", { id: "slider12c", min: 0, max: 10, range: true, value: [3, 7] });


function readURL(input, target) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(target).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }

}
