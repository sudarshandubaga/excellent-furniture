		</div>
		</div>
	</div>
</div>
		{{ HTML::script('js/jquery.min.js') }}
		{{ HTML::script('js/popper.min.js') }}
	    {{ HTML::script('js/bootstrap.min.js') }}
	    {{ HTML::script('js/sweetalert.min.js') }}
	    {{ HTML::script('js/validation.js') }}
		{{ HTML::script('admin/js/jquery.multiselect.js') }}
	    {{ HTML::script('admin/js/jquery.multiselect.js') }}
	    {{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
	    {{ HTML::script('admin/js/main.js') }}

	    <script type="text/javascript">
	    	$(function() {
	    		$('select[multiple].active.3col').multiselect({
		            columns: 1,
		            placeholder: 'Select Courses',
		            search: true,
		            searchOptions: {
		                'default': 'Search Courses'
		            },
		            selectAll: true
		        });

		        $('select[multiple].active.color').multiselect({
		            columns: 1,
		            placeholder: 'Select Color',
		            search: true,
		            searchOptions: {
		                'default': 'Search Color'
		            },
		            selectAll: true
		        });

				$('select[multiple].active.product').multiselect({
		            columns: 1,
		            placeholder: 'Select Products',
		            search: true,
		            searchOptions: {
		                'default': 'Search Products'
		            },
		            selectAll: true
		        });

		        $('select[multiple].active.age').multiselect({
		            columns: 1,
		            placeholder: 'Select Age',
		            search: true,
		            searchOptions: {
		                'default': 'Search Age'
		            },
		            selectAll: true
		        });
	    	});
	    </script>
	</body>
</html>
