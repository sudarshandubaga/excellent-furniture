<section class="inner-part">
	<div class="row color">
		<div class="col">
			<h3 class="pb-2">View Products</h3>
			<div class="divider"></div>
			<div class="content-part">
				@if (\Session::has('success'))
				    <div class="alert alert-success">
					    {!! \Session::get('success') !!}</li>
					</div>
				@endif
				<form method="post">
					@csrf
					<div class="product">
						<div class="heading">
							<h5>{{ $records->count() }} record(s) found</h5>
							@if(!$records->isEmpty())
							<a href="" class="icon-trash-o"></a>
							@endif
						</div>
						<div class="divider"></div>
						@if(!$records->isEmpty())
						<div class="table-responsive">
							<table class="table table-bordered table-striped text-center">
								<thead>
									<tr>
										<th>
											<label class="animated-checkbox">
		                                        <input type="checkbox"  class="check_all">
		                                        <span></span>
		                                    </label>
										</th>
										<th>SN.</th>
										<th>Image</th>
										<th>Title</th>
										<th>Code</th>
										<th>Category</th>
										<th>Subcategory</th>
										<th>Price</th>
										<th>Sort By</th>
										<th>In Stock</th>
										<th>Home</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@php $sn = 0; @endphp
									@foreach($records as $rec)
									<tr>
										<td>
											<label class="animated-checkbox">
		                                        <input type="checkbox" name="check[]" class="check" value="{{ $rec->product_id }}">
		                                        <span></span>
		                                    </label>
										</td>
										<td>{{ ++$sn }}.</td>
										<td>
											<img src="{{ !empty($rec->product_image) ? url('imgs/product/'.$rec->product_image) : url('imgs/no-image.png') }}" style="width: 100px;">
										</td>
										<td>{{ $rec->product_name }}</td>
										<td>{{ $rec->product_code }}</td>
										<td>{{ $rec->mcategory }}</td>
										<td>{{ $rec->scategory }}</td>
										<td class="nowrap">
											INR: {!! $rec->product_sell_price !!}<br>
											USD: {!! $rec->product_sell_price_dollar !!}
										</td>
										<td>
											<input type="number" name="sort_by[{{ $rec->product_id }}]" value="{{ $rec->product_order }}" class="form-control porder">
										</td>
										<td class="text-center">
											<a href="{{ url('rt-admin/product?stock='.$rec->product_in_stock.'&id='.$rec->product_id) }}" class="{{ $rec->product_in_stock == 'Y' ? 'text-success': 'text-danger' }}">
												<i class="icon-circle"></i>
											</a>
										</td>
										<td class="text-center">
											<a href="{{ url('rt-admin/product?view='.$rec->product_is_home.'&id='.$rec->product_id) }}" class="{{ $rec->product_is_home == 'Y' ? 'text-success': 'text-danger' }}">
												<i class="icon-circle"></i>
											</a>
										</td>
										<td>
											<a href="{{ url('rt-admin/product/add/'.$rec->product_id) }}" title="Edit" class="text-success">
	                                        	<i class="icon-pencil"></i>
	                                        </a>&nbsp;
											<a href="{{ url('rt-admin/product?status='.$rec->product_is_visible.'&id='.$rec->product_id) }}" class="{{ $rec->product_is_visible == 'Y' ? 'text-success': 'text-danger' }}">
												<i class="icon-circle"></i>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						{{ $records->links() }}
						@else
						<div class="alert alert-warning text-center"> <i class="icon-thumbs-o-down"></i> No records found.</div>
						@endif
					</div>
				</form>
			</div>
		</div>
</div>
</section>
