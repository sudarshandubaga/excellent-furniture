<div class="blog-border">
     <div class="text-center">
        {{ date("F d, Y", strtotime($blog->post_created_on)) }}
     </div>

     <div class="blog-name">
         {{ $blog->post_title }}
     </div>

     <div class="blog-des">
           {{ substr( strip_tags($blog->post_description) , 0, 216) }}
           @if(strlen( strip_tags($blog->post_description) ) > 216)
           &hellip;
           @endif
     </div>

     <a href="{{ url('blog/'.$blog->post_slug) }}">
         Read More <i class="icon-keyboard_arrow_right"></i>
     </a>
</div>
