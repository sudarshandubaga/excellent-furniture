<footer>
     <div class="container">
         <ul class="footer-nav">
             <li><a href="{{ url('contact') }}">CONTACT US</a></li>
             <li><a href="{{ url('page/shipping-delivery') }}">SHIPPING & DELIVERY</a></li>
             <li><a href="{{ url('page/return-refunds') }}">RETURNS & REFUNDS</a></li>
             <li><a href="{{ url('page/terms-condition') }}">TERMS & CONDITION</a></li>
             <li><a href="{{ url('page/privacy-policy') }}">PRIVACY POLICY</a></li>
             <li><a href="{{ url('wholesale') }}">WHOLESALE</a></li>
             <li><a href="{{ url('blog') }}">BLOG</a></li>
         </ul>

         <div class="follow">
             follow us
         </div>

         <div class="social-icons">
             <ul class="social-hub">
                 <li><a href="{{ $site->setting_instagram }}" target="_blank"><i class="icon-instagram"></i></a></li>
                 <li><a href="{{ $site->setting_facebook }}" target="_blank"><i class="icon-facebook"></i></a></li>
             </ul>
         </div>

         <div class="rights">
            <i class="icon-copyright1"></i>2018 Toy Ecommerce. All Rights Reserved.
         </div>
     </div>

    <a href="" id="return"> <i class="icon-keyboard_arrow_up"></i> </a>
</footer>

<div class="push-notification">
    <a href="#" class="close">&times</a>
    <div class="pn-message">
        Joe Due from Rajasthan, India has just purchased <strong>Rainbo semi circle set</strong>
    </div>
</div>

{{ HTML::script('js/jquery.min.js') }}
{{ HTML::script('js/multirange.js') }}
{{ HTML::script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
<!-- {{ HTML::script('js/bootstrap-slider.min.js') }} -->
<!-- {{ HTML::script('js/xzoom.min.js') }} -->
{{ HTML::script('js/validation.js') }}
<!-- {{ HTML::script('js/setup.js') }} -->
{{ HTML::script('js/sweetalert.min.js') }}
{{ HTML::script('js/owl.carousel.min.js') }}
{{ HTML::script('js/star-rating.js') }}
{{ HTML::script('js/select2.min.js') }}
{{ HTML::script('https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js') }}
{{ HTML::script('js/custom.js') }}

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116196832-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116196832-1');
</script>

<script>
    $(function () {
        $('.rating').rating({
            containerClass: 'is-star',
            step: 1
        });

        $('.select2').select2();
    })
</script>
</body>
</html>
