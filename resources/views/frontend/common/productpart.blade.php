@php

    $session_auth = session('user_auth');

@endphp



<div class="toy-border mb-3">

    <a href="{{ url('product/'.$pro->product_slug) }}">

        <img src="{{ url('imgs/product/'.$pro->product_image_medium) }}" class="toy">

    </a>



    <div class="toy-info">

        <div class="product-info-content">

            <a href="{{ url('product/'.$pro->product_slug) }}">

                {{ $pro->product_name }}

            </a>

            <div class="second-info">

                <!-- {{ $pro->product_category }} -->

            </div>



            @php

                $exist = DB::table('wishlist')->where('wish_uid', $session_auth)->where('wish_pid', $pro->product_id)->first();

                $avgrating = DB::table('reviews')->where('review_pid', $pro->product_id)->select(DB::raw('AVG(review_rating) AS avg'))->first();
                
                $ratingcount = DB::table('reviews')->where('review_pid', $pro->product_id)->count();

            @endphp

            @if(!empty($ratingcount))

            <span style="color: #4d4d4d;">

                @for($i = 1; $i <= $avgrating->avg; $i++)

                <i class="icon-star"></i>

                @endfor

                @php

                $arr = explode(".", $avgrating->avg);

                @endphp

                @if(!empty($arr[1]))

                @php $i++ @endphp

                <i class="icon-star_half"></i>

                @endif

                @for($j = $i; $j <= 5; $j++)

                 <i class="icon-star_outline"></i>

                @endfor

            </span>

            <span style="color: #6b6b6b;">{{ $ratingcount }}</span>

            @endif



            <div class="product-price" data-usd="{{ $pro->product_sell_price_dollar }}" data-inr="{{ $pro->product_sell_price }}" style="color: #05d6ac;">

                {{ $country_code == 'IN' ? '₹ '.$pro->product_sell_price : '$ '.$pro->product_sell_price_dollar }}

            </div>

            @if(empty($ratingcount))

            <div style="margin-top:24px;"></div>

            @endif

        </div>

        <div class="heart">

            @if(!empty($session_auth))

                <i class="@if(empty($exist)) icon-heart-o @else icon-heart @endif wishlist" data-id="{{ $pro->product_id }}" data-url="{{

                url('ajax/wishlist')}}" ></i>

               @else

                <i></i>

            @endif

        </div>





        @if($pro->product_in_stock == 'N')

            <div class="out_of_stock">

                <div class="text">

                    OUT OF STOCK

                </div>

            </div>

            <div class="btn-group btn_group">

                <a href="{{ url('product/'.$pro->product_slug) }}" class="btn-to text-center">NOTIFY ME</a>

            </div>

        @else

            <div class="btn-group btn_group">

                <button type="button" class="btn-to add_to_cart" data-url="{{ url('ajax/add-to-cart')

            }}" data-id="{{ $pro->product_id }}">ADD TO CART</button>

                <button type="button" class="btn-to buy_now_btn" data-url="{{ url('ajax/add-to-cart')

            }}" data-id="{{ $pro->product_id }}">BUY NOW</button>

            </div>

        @endif

    </div>

</div>

