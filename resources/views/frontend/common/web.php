<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Backend Routes

Route::get('rl-xpanel', 'admin\Dashboard@index');

Route::any('rl-xpanel/book', 'admin\Book@index');
Route::any('rl-xpanel/book/add/{nom?}', 'admin\Book@add');

Route::any('rl-xpanel/faq', 'admin\Faq@index');
Route::any('rl-xpanel/faq/add/{nom?}', 'admin\Faq@add');

Route::any('rl-xpanel/page', 'admin\Pages@index');
Route::any('rl-xpanel/page/add/{nom?}', 'admin\Pages@add');

Route::any('rl-xpanel/users', 'admin\Users@index');
Route::any('rl-xpanel/reviews/{nom?}', 'admin\Review@index');
// Route::any('rl-xpanel/user/{nom}', 'admin\User@index');

Route::any('rl-xpanel/topic/{nom?}', 'admin\Topic@index');

Route::any('rl-xpanel/question', 'admin\Question@index');
Route::any('rl-xpanel/question/add/{nom?}', 'admin\Question@add');

Route::any('rl-xpanel/paragraph', 'admin\Paragraph@index');
Route::any('rl-xpanel/paragraph/add/{nom?}', 'admin\Paragraph@add');

Route::any('rl-xpanel/blog', 'admin\Blog@index');
Route::any('rl-xpanel/blog/add/{nom?}', 'admin\Blog@add');

Route::any('rl-xpanel/category/{nom?}', 'admin\Category@index');
Route::any('rl-xpanel/reading/add/{nom?}', 'admin\Reading@add');

Route::any('rl-xpanel/setting', 'admin\Setting@index');
Route::any('rl-xpanel/change-password', 'admin\User@change_password');
Route::any('rl-xpanel/logout', 'admin\User@logout');


Route::post('rl-xpanel/ajax/user_login', 'admin\Ajax@user_login');
Route::post('rl-xpanel/ajax/upload-image', 'admin\Ajax@upload_image');
Route::post('rl-xpanel/ajax/{action}', 'admin\Ajax@index');

// Frontend Routes
Route::get('/', 'HomeController@index');
Route::get('contact', 'ContactController@index');
Route::get('blog', 'BlogController@index');
Route::get('blog/{slug}', 'BlogController@single');
Route::get('english', 'EnglishController@index');
Route::get('question/{course}/{subject}/{topic}', 'Question@index');
Route::get('reading', 'Reading@index');

// Courses and subjects
$courses =  DB::table('courses')
            ->where('course_is_deleted', 'N')
            ->where('course_is_visible', 'Y')
            ->select('course_id','course_slug')
            ->get();

foreach($courses as $course) {
	$subjects = DB::table('subjects')
				->where('subject_is_deleted', 'N')
				->where('subject_is_visible', 'Y')
				->where('subject_course', $course->course_id)
				->select('subject_id','subject_slug')
				->get();

	foreach($subjects as $subj) {
		$cslug = $course->course_slug;
		$sslug = $subj->subject_slug;
		Route::get('{'.$cslug.'}'.'/'.'{'.$sslug.'}', [
			'as'	=> 'topic',
			'uses'	=> 'Topic@index'
		]);
	}
}