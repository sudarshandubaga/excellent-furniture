<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">About-Us</li>
        </ol>
    </nav>


    <section class="first-section">
        <div class="about-head">
            Toy Ecommerce
        </div>

        <div>
            <img src="imgs/logo.png" class="about-img">
        </div>

        <div class="about-line">
            Who we are
        </div>

        <p class="about-next-line">
            Toy Ecommerce is sustainably managed families run wooden toys brand based in The Thar Desert of India. We mainly focuses on offering safe wooden toys made of sustainably managed woods that compliment development for children, in particularly during their first five formative years when parents should reassure children to sense the privilege of playing and the proximity to nature.
        </p>

        <p class="about-next-line">
            With a strong mission to create a sustainable world, Toy Ecommerce thoroughly concerns about every single step of production line from choosing the apt material to packaging as well as other activities as we are wholeheartedly committed to minimizing environmental impacts.
        </p>
    </section>

    <section class="first-section">
        <div class="about-line">
            What we create
        </div>
        <p class="about-next-line">
            We create beautiful and functional toys to enhance Childs development skills and inspire them to imagine, to create and to discover.
        </p>
        <p class="about-next-line">
            <strong>Toy Ecommerce's</strong> company philosophy is based on a simple principle. Children are the most important part in life. We believe that future begins and belong to our children. It is our responsibility to provide them with endless possibilities, not unsolvable problems. This philosophy makes us committed to enriching the lives of children with durable toys that has simple and minimalist shapes combined with beautiful colors and inviting feel.
        </p>

        <p class="about-next-line">
            From design perspective pedagogical approaches such as Waldorf education and Montessori methods are the basis of our ideas, we try to keep the detailing minimal and playing possibility open as possible so that when combined with Childs imagination, it become a tool of exploration of the world they live in and a perfect companions on their way through childhood.
        </p>

        <p class="about-next-line">
            Open-endedness of our toys makes them appropriate for all the ages and inspire time and again for new game play. A toy object may become a doll house and maybe a farm next time. Reduced design of toys encourages the intuitive play pattern beyond the barriers of language, culture and gender.
        </p>

        <p class="about-next-line">
            Toy Ecommerce Toys are used as pedagogical aids in kindergartens, which find applications for educational and for rehabilitation purposes. What children experience as fun, parents and educationist perceive as age-appropriate learning basics; sociable, Intellectual, physical, and Language skills.
        </p>
    </section>

    <section class="first-section">
        <div class="about-line">
            How we make it
        </div>

        <p class="about-next-line">
            The entire Toy Ecommerce collection is designed and produced in our facility. With a few exceptions, everything is sawn and sanded by hand- Which carries the uniqueness in each piece and gives a special expression to the toy than pure “Machine made”.
        </p>

        <p class="about-next-line">
            At Toy Ecommerce we believe that products are only as good as the material used to make them and the quality of their workmanship. Wood being a natural material makes a toy easy, strong and safe and unlike mass produced plastic toys, each one a subtly unique with unique traces of nature.
        </p>

        <p class="about-next-line">
            Our commitment to a better future for next generation and responsibility toward environment is our far most priority. That is why we pay special attention to the quality of the material we use. All the raw material used is first carefully inspected and go through our quality assurance before they are approved for production. We are very particular in choosing our supplier as well and we partner with those who share our vision.
        </p>

        <p class="about-next-line">
            All our toys are made of European beech, American maple which is sourced from FSC certified supplier and mango which is harvested for timber once stop ripping fruits and give place for a new tree. The Surface finishes, color dyes are all environments friendly and water based that comply with the European toy safety standard (EN-71) and US standard (ASTM).
        </p>
    </section>


    <section class="responsible">
          <div class="res-head">
              RESPONSIBLE RESOURCES
          </div>

          <div class="res-desc">
              Resources should be sourced and used responsibly, so they will still benefit future
              generations. We are constantly in pursuit of more sustainable solutions to our raw
              materials consumption and our packaging. We also work towards improving our waste
              management and eliminating waste in our production.
          </div>

          <div class="row">
              <div class="col-xl-4">
                  <div>
                      <img src="imgs/NON-TOXIC-PAINTS.jpg" class="res-mg">
                  </div>

                  <p class="res-name">
                      NON TOXIC PAINTS
                  </p>

                  <div class="res-desc-inner">
                      When we add those beautiful colors to our toys, we use solvent free water
                      based paints which is non toxic. So there is no worry if babies put them in
                      their mouth. All coatings of our toys come from highly specialized vendors
                      and they are certified in accordance with most stringent EN-71 European
                      Saftey norms.
                  </div>
              </div>

              <div class="col-xl-4">
                  <div>
                      <img src="imgs/SUSTAINABLE-WOOD.jpg" class="res-mg">
                  </div>

                  <p class="res-name">
                      SUSTAINABLE WOOD
                  </p>

                  <div class="res-desc-inner">
                      We source wood from FSC certified supplier only, Which make sure that the                         wood has come from sustainably managed forest. Mostly we use European
                      Beach wood or mango wood that no longer produce Fruit and harvested for
                      timber. By only utilizing wood from sustainable forest we help preserve the
                      ecological balance of our planet’s forest and do our part in improving
                      sustainable forestry practices.
                  </div>
              </div>

              <div class="col-xl-4">
                  <div>
                      <img src="imgs/MINIMAL-PACKAGING.jpg" class="res-mg">
                  </div>

                  <p class="res-name">
                      MINIMAL PACKAGING
                  </p>

                  <div class="res-desc-inner">
                      All products are packaged in 100% recyclable cardboard to minimize
                      environmental impact and maximize efficiency. For us it is important to
                      achieve minimal packaging because we do not want to contribute to worldwide
                      packaging waste any more than absolutely necessary to ensure safe transport.
                  </div>
              </div>
          </div>
              <section class="about-resposible">
                  <div class="row">
                      <div class="col-xl-4">
                          <div>
                              <img src="imgs/QUALITY-TESTING.jpg" class="res-mg">
                          </div>

                          <p class="res-name">
                              QUALITY TESTING
                          </p>

                          <div class="res-desc-inner">
                              Our toys undergo a lengthy and complex test to guarantee CHILDREN’s
                              complete safety. Our internal management has been integrated into every
                              process of the products invention and production. They surpass all
                              international safety standards including both ASTM and EN71.
                          </div>
                      </div>

                      <div class="col-xl-4">
                          <div>
                              <img src="imgs/NON-TOXIC-PAINTS.jpg" class="res-mg">
                          </div>

                          <p class="res-name">
                              PRINTING WITH SOY INK
                          </p>

                          <div class="res-desc-inner">
                              for Printed material. We use soy ink which is more readily
                              biodegradable and can be recycled more efficiently than standard
                              chemical ink.
                          </div>
                      </div>

                      <div class="col-xl-4">
                          <div>
                              <img src="imgs/Reforestation-Program.jpg" class="res-mg">
                          </div>

                          <p class="res-name">
                              REFORESTATION PROGRAM
                          </p>

                          <div class="res-desc-inner">
                              By collaborating with social organization we arrange an annual plan
                              “Toy Ecommerce loves the forest Program” for replantation and to spread
                              awareness. To make sure that the world always remains a colorful and
                              liveable place Toy Ecommerce takes o responsibility for sustainability.
                          </div>
                      </div>
                  </div>
              </section>
          </div>
    </section>
</div>
