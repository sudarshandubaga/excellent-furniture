@php
	$session_auth = session('user_auth');
@endphp

@if(!empty($session_auth))
    @include('frontend.inc.'.$page2)
@else
	@include('frontend.inc.login')
@endif