<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Blog</li>
        </ol>
    </nav>

    <div class="blog-main-head">
         blog
    </div>


        <div class="row">
            @foreach($blogs as $blog)
                <div class="col-xl-4">
                    @include('frontend.common.blogpart')
                </div>
            @endforeach
        </div>

</div>
