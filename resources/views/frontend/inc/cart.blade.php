@php $total = 0; @endphp
@foreach($cartArr as $cart)
@php
    if($country_code == 'IN') {
        $total += $cart->product_sell_price * $cart->qty;
    } else {
        $total += $cart->product_sell_price_dollar * $cart->qty;
    }
@endphp
@endforeach
<section class="cart-height">
    <div class="container">
        <!-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Cart</li>
            </ol>
        </nav> -->
        <div class="clearfix">
            @if(!empty($cartArr))
            <div class="float-right cart-right-header">
                <div class="float-left text-right cart-right-header-left">
                    <small>Subtotal</small>
                    <div class="">
                        {{ $country_code == 'IN' ? '₹ '.$total : '$ '.$total }}
                    </div>
                </div>
                <div class="float-right ">
                    <a href="{{ url('checkout') }}" class="checkout header-checkout btn">
                        <i class="icon-cart"></i>&nbsp; Checkout
                    </a>
                </div>
            </div>
            @endif
            <h1 class="cart-heading mt-3">your cart</h1>
        </div>
        @if(!empty($cartArr))
            <div>
                <div id="cartProducts">
                    @include('frontend.inc.cart_product')
                </div>
            </div>
        @else
         <div class="no_records_found">
             <div class="no_record_icon">
                 <i class="icon-cart"></i>
             </div>
             You cart is empty
         </div>
       @endif
  </div>
</section>
