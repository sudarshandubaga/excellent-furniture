<section class="side-cart">
    @foreach($cartArr as $cart)
        @php $totalprice = $cart->product_sell_price * $cart->qty; @endphp
        <div class="cart-product-row">
            <div class="row">
                <div class="col-md-8 clearfix">
                    <div class="float-left column-mobile-3">
                        <img src="{{ url('imgs/product/'.$cart->product_image) }}" class="toy-one">
                    </div>
                    <div class="mt-4 mb-4 column-mobile-9  float-left">
                        <h4><strong>{{ $cart->product_name }}</strong></h4>
                        <div class="price">
                            <strong>Price:</strong> {{ $country_code == 'IN' ? '₹ '.$cart->product_sell_price : '$ '.$cart->product_sell_price }}
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-6">
                    <div class="cart_margin_top">
                        <!-- Qty: -->
                        <div class="qty-product d-inline-block" style="height: auto; max-width: 120px; padding: 5px;">
                            <div class="increment input-group">
                                <div class="input-group-prepend">
                                  <button class="add minus update_cart_value cart_qty_input" type="button">-</button>
                                </div>
                                <input type="text" data-pid="{{ $cart->product_id }}" data-action="{{ url('ajax/update') }}" value="{{ $cart->qty }}" name="noOfRoom" class="text-center txt_wth qty_input custom num-filled form-control" />
                                <div class="input-group-append">
                                  <button class="odd plus update_cart_value cart_qty_input" type="button">+</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-4 text-center">
                    <div class="cart_margin_top price-right">
                        <span>{{ $country_code == 'IN' ? '₹ '.$cart->product_sell_price * $cart->qty : '$ '.$cart->product_sell_price_dollar * $cart->qty }}</span>
                    </div>
                </div>

                <div class="col-md-1 col-2 text-center">
                    <div class="cart_margin_top">
                        <span class="cart-remove-icon"><i class="icon-clear remove_item_from_cart" data-id="{{ $cart->product_id }}" data-url="{{ url('ajax/remove-to-cart/') }}"> </i></span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</section>
<div class="row">
    <div class="col-6 cart-subtotal">
          <div class="ammount_lable">Subtotal: </div>
    </div>

    <div class="col-6 cart-subtotal">
        <div class="amt_lable money">{{ $country_code == 'IN' ? '₹ '.$total : '$ '.$total }}</div>
    </div>
</div>

<div class="row">
   <div class="col-6 cart-subtotal">
         <small class="ammount_lable mt-3">Shipping: </small>
   </div>
   <div class="col-6 cart-subtotal">
       <small class="amt_lable mt-3">Free</small>
   </div>
</div>

<div>
  <a href="{{ url('checkout') }}"  class="btn btn-lg btn-block checkout text-center text-white">
      <i class="icon-cart"></i> &nbsp; Checkout
  </a>
</div>
