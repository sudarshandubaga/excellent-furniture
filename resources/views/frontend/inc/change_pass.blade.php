<section class="profile-block wishlist">
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                @include('frontend.common.profile_side')

                <div class="col-xl-8">
                    <div class="Change_Pass">
                        <h3>Change Password</h3>
                        <hr>
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                {!! \Session::get('success') !!}</li>
                            </div>
                        @endif
                        @if (\Session::has('failed'))
                            <div class="alert alert-danger">
                                {!! \Session::get('failed') !!}</li>
                            </div>
                        @endif
                        <label class="mt-3 mb-2">Basic Informations</label>
                        <div class="form-group">
                            <input type="password" class="form-control field-shadow" placeholder="Current password" name="password[current]" value="" required>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control field-shadow" placeholder="New password" name="password[new]" value="" required>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control field-shadow" placeholder="Confirm password" name="password[confirm]" value="" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="save-button form-control">SAVE</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
