<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
        </ol>
    </nav>

    <form method="post">
        @csrf
        <div class="row">
            <div class="col-xl-8">
                <div class="checkout-step">
                    <h3 class="heading">
                        <span class="step-count">
                            1
                        </span>

                        <span class="login-spell">LOGIN</span>
                        <i class="icon-checkmark"></i>

                        <a href="{{ url('logout') }}" class="logout-anker">
                            LOGOUT
                        </a>
                    </h3>

                    <div class="step-body">
                        {{ $record->user_name }}&nbsp;&nbsp;&nbsp;<span><b>{{ $record->user_email }}</b></span>
                        <div class="text-right">
                            <button type="button" class="next-btn btn btn-primary">Next</button>
                        </div>
                    </div>
                </div>


                <div class="checkout-step">
                    <h3 class="heading">
                        <span class="step-count">
                            2
                        </span>

                        <span class="login-spell">DELIVERY ADDRESS</span>
                    </h3>

                    <div class="step-body">
                        @foreach($addresses as $add)
                        <div class="row">
                            <div class="radio-jockey">
                                <input type="radio" class="addresses" value="{{ $add->add_id }}" name="record[address]">
                            </div>


                            <div class="col-xl-9">
                                <div class="name-num">
                                    <span><b>{{ $record->user_name }}</b></span>&nbsp;
                                    <span><b>{{ $record->user_mobile }}</b></span>
                                </div>
                                <div class="first-address">
                                    {{ $add->add_line1 }}, {{ $add->add_line2 }}, {{ $add->add_country }}, {{ $add->add_state }} ({{ $add->add_pincode }})
                                </div>

                                <a href="" class="dh">
                                    DELIVER HERE
                                </a>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                        <!-- <input type="hidden" id="Address" name="record[address]" value=""> -->

                        <div class="row">
                            <div class="col">
                                <button type="button" class="prev-btn btn btn-primary">Previous</button>
                            </div>
                            <div class="col text-right">
                                <button type="button" class="next-btn btn btn-primary">Next</button>
                            </div>
                        </div>

                        <div class="add-new">
                            <i class="icon-plus1"></i>
                            <a href="" class="aand">Add a new address</a>
                        </div>
                    </div>
                </div>

                <div class="checkout-step">
                     <h3 class="heading">
                         <span class="step-count">
                            3
                        </span>

                        <span class="login-spell">ORDER SUMMARY</span>
                    </h3>

                    <div class="step-body">
                         @include('frontend.inc.cart_product')

                        <div class="row">
                            <div class="col">
                                <button type="button" class="prev-btn btn btn-primary">Previous</button>
                            </div>
                            <div class="col text-right">
                                <button type="button" class="next-btn btn btn-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="checkout-step">
                    <h3 class="heading">
                        <span class="step-count">
                            4
                        </span>

                        <span class="login-spell">PAYEMENT OPTIONS</span>
                    </h3>

                    <div class="step-body">
                        <div class="cod">
                            <input type="radio" name="record[payment]" value="Cash on Delivery">&nbsp;&nbsp;

                            <span>Cash on Delivery</span>
                        </div>

                        <div class="op">
                            <input type="radio" name="record[payment]" value="Online-Payment">&nbsp;&nbsp;

                            <span>Online-Payment</span>
                        </div>

                        <div class="row">
                            <div class="col">
                                <button type="button" class="prev-btn btn btn-primary">Previous</button>
                            </div>
                            <div class="col text-right">
                                <button type="submit" class="finish-btn btn btn-primary">Finish</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-xl-4">
                <div class="delivery-one">
                    <span class="login-spell">PRICE DETAILS</span>
                </div>

                <div class="total-payable">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="ammount_lable">Total: </div>
                           <div class="ammount_lable mt-3">Shipping Charge : </div>
                            <div class="ammount_lable mt-3">Grand Total : </div>
                        </div>
                        <div class="col-sm-5 text-right">
                            <div class="amt_lable">Rs {{ $total }} </div>
                            <div class="amt_lable mt-3">Free</div>
                            <div class="ammount_lable mt-3">Rs {{ $total }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
