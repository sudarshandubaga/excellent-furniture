@php
$cart_session = session('cart_session');
$cart_session = !empty($cart_session) ? $cart_session : array();
@endphp
<div class="container">
    <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Checkout</li>
        </ol>
    </nav> -->
    @if(!empty($cart_session))
        <form id="checkoutForm" method="post">
            @csrf
            @if(!empty($record->user_name))
            <p style="border-bottom: 2px solid #ebebeb; padding-bottom: 5px;">You've logged in as {{ @$record->user_name }}</p>
            @else
            <p style="border-bottom: 2px solid #ebebeb; padding-bottom: 5px">Already have an account? <a href="{{ url('user/?redirect_url='. urlencode( url('checkout') ) ) }}">Log in</a> </p>
            @endif
            <div class="row">
                <div class="col-lg-6">
                    <h3>Shipping Address</h3>
                    <div class="form-group">
                        <label class="xbox">&nbsp;</label>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="record[order_shipping][fname]" value="{{ @$record->user_fname }}" class="form-control name" placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="record[order_shipping][lname]" value="{{ @$record->user_lname }}" class="form-control name" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][company]" value="" class="form-control" placeholder="Company (optional)">
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][address1]" value="" class="form-control" placeholder="Address 1 (Max. 80 characters)" maxlength="80" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][address2]" value="" class="form-control" placeholder="Address 2 (optional) (Max. 80 characters)" maxlength="80">
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][city]" value="" class="form-control" placeholder="City" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control select2" name="record[order_shipping][country]" id="shipping_country" required>
                                    <option value="">Select Country / Region</option>
                                    @foreach($countries as $con)
                                    <option value="{{ $con->country_code }}" @if($country_code == $con->country_code) selected @endif>{{ $con->country_name.' ('.$con->country_code.')' }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" name="record[order_shipping][state]" value="" class="form-control" placeholder="State" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" name="record[order_shipping][postcode]" value="" class="form-control" placeholder="Postcode" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][phone]" value="{{ @$record->user_mobile }}" class="form-control" placeholder="Phone" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_shipping][email]" value="{{ @$record->user_email }}" class="form-control" placeholder="Email" required>
                    </div>
                    @if(empty($record->user_id))
                    <div class="form-group">
                        <label class="xbox">
                            <input type="checkbox" name="create_an_account" value="yes">
                            <span class="checkmark"></span>
                            <span class="name">Create an account?</span>
                        </label>
                    </div>
                    <div class="form-group">
                        <input type="password" name="user_password" value="" class="form-control" placeholder="Create email password">
                    </div>
                    @endif
                </div>
                <div class="col-lg-6">
                    <h3>Billing Address</h3>
                    <div class="form-group">
                        <label class="xbox">
                            <input type="checkbox" id="billing_same_as_shipping">
                            <span class="checkmark"></span>
                            <span class="name">Same as Shipping Address</span>
                        </label>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="record[order_billing][fname]" value="{{ @$record->user_fname }}" class="form-control name" placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="record[order_billing][lname]" value="{{ @$record->user_lname }}" class="form-control name" placeholder="Last Name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][company]" value="" class="form-control" placeholder="Company (optional)">
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][address1]" value="" class="form-control" placeholder="Address 1 (Max. 80 characters)" maxlength="80" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][address2]" value="" class="form-control" placeholder="Address 2 (optional) (Max. 80 characters)" maxlength="80">
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][city]" value="" class="form-control" placeholder="City" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control select2" name="record[order_billing][country]" required>
                                    <option value="">Select Country / Region</option>
                                    @foreach($countries as $con)
                                    <option value="{{ $con->country_code }}" @if($country_code == $con->country_code) selected @endif>{{ $con->country_name.' ('.$con->country_code.')' }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" name="record[order_billing][state]" value="" class="form-control" placeholder="State" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" name="record[order_billing][postcode]" value="" class="form-control" placeholder="Postcode" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][phone]" value="{{ @$record->user_mobile }}" class="form-control" placeholder="Phone" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="record[order_billing][email]" value="{{ @$record->user_email }}" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <textarea name="record[order_message]" rows="3" class="form-control" placeholder="Leave a note or gift message (optional)"></textarea>
                    </div>
                </div>
            </div>

            <h3 style="border-bottom: 2px solid #ebebeb;" class="mb-5">Your Order</h3>
            <div class="row" id="checkout_your_order">
                @include('frontend.template.checkout_your_order')
            </div>

        </form>
    @else
        <div class="no_records_found">
            <div class="no_record_icon">
                <i class="icon-cart"></i>
            </div>
            You cart is empty
        </div>
    @endif
</div>
