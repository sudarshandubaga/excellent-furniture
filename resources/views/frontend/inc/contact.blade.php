<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
    </nav>

    <section class="contact-block">
        <div class="row">
            <div class="col-xl-4">
                <div class="contact-border">
                    <div class="contact-first">
                        contact
                    </div>

                    <div class="row">
                        <div class="col-xl-1">
                            <i class="icon-location_on"></i>
                        </div>

                        <div class="col-xl-11">
                            <div class="contact-address">
                                {{ $site->setting_address }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contact-border">
                     <div class="row">
                          <div class="col-xl-1">
                               <i class="icon-envelope"></i>
                          </div>

                          <div class="col-xl-11">
                              <div>
                                  {{ $site->setting_contact_email }}
                              </div>
                          </div>
                     </div>
                </div>

                <div class="contact-border">
                    <div class="row">
                        <div class="col-xl-1">
                            <i class="icon-phone"></i>
                        </div>

                        <div class="col-xl-11">
                            <div>
                                {{ $site->setting_mobile }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="social-icons contact-right">
                    <ul class="social-hub contact-link-hub">
                        <li><a href="{{ $site->setting_instagram }}" target="_blank"><i class="icon-instagram"></i></a></li>
                        <li><a href="{{ $site->setting_facebook }}" target="_blank"><i class="icon-facebook"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="first-para">
                     Welcome to Chitrani.com - your place for handmade wooden toys. We are located in
                     Jodhpur, India. We are very passionate about our family run and owned business
                     and are dedicating everything to make your shopping experience with us as good
                     as possible.
                </div>

                <div class="second-para">
                    We would love to hear from you in any shape or form so please feel free to send
                    us a message and we will do our very best to help and assist you as fast as poss
                    ible
                </div>
            </div>

            <div class="col-xl-4">
                {{ Form::open(array('method' => 'post')) }}
                <div class="contact-right">
                    <div class="get-head">
                         get in touch with {{ $site->setting_title }}
                    </div>
                    @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}</li>
                        </div>
                    @endif
                    @if (\Session::has('danger'))
                        <div class="alert alert-danger">
                            {!! \Session::get('danger') !!}</li>
                        </div>
                    @endif
                    <div class="query-send">
                        Send us your query, we will reply within next 24hrs!
                    </div>

                    <div class="row form-group">
                        <div class="col-xl-4">
                            <div>
                                Your Name:
                            </div>
                        </div>

                        <div class="col-xl-8">
                            <input type="text" name="contact[name]" class="form-control black-box" required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xl-4">
                            <div>
                                Your Email:
                            </div>
                        </div>

                        <div class="col-xl-8">
                            <input type="email" name="contact[email]" class="form-control black-box" required>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-xl-4">
                            <div>
                                Your Message:
                            </div>
                        </div>

                        <div class="col-xl-8">
                            <textarea cols="26" name="contact[message]" rows="6" class="form-control black-box" required></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-4"></div>
                        <div class="col-xl-8">
                            <div class="form-group">
                                <img src="{{ url('captcha-code').'?v='.time() }}" alt="" class="captcha-image">
                                <i class="icon-refresh refresh-captcha"></i>
                            </div>
                        </div>
                        <div class="col-xl-4"></div>
                        <div class="col-xl-8">
                            <div class="form-group">
                                <input type="text" name="captcha" value="" placeholder="Enter Captcha Code" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="row form-group text-center">
                        <div class="col-xl-4"> </div>
                        <div class="col-xl-8 text-left">
                            <button type="submit" class="btn mail-submit btn-block">
                                SEND EMAIL
                            </button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </section>
</div>
