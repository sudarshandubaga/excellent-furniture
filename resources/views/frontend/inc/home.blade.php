@if(!$sliders->isEmpty())
<div id="demo" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
        @php $sn = 0; @endphp
        @foreach($sliders as $slide)
            <li data-target="#demo" data-slide-to="{{ $sn }}"@if($sn == 0) class="active"@endif></li>
            @php $sn++; @endphp
        @endforeach
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
        @php $sn = 0; @endphp
        @foreach($sliders as $slide)
            @php
            $sn++;
            @endphp
        <div class="carousel-item @if($sn == 1) active @endif">
            <img src="{{ url('imgs/sliders/'.$slide->slider_image) }}" style="width: 100%;" alt="{{ $slide->slider_title }}" title="{{ $slide->slider_title }}">
            <!-- <div class="container">
                 <div class="img-headline">
                    {{ $slide->slider_title }}
                 </div>
                 <div class="fbs">
                    {{ $slide->slider_subtitle }}
                 </div>
            </div> -->
        </div>
        @endforeach
    </div>

    <!-- Left and right controls -->
    <a class="leftr" href="#demo" data-slide="prev">
        <span class="icon-keyboard_arrow_left"></span>
    </a>
    <a class="leftr" href="#demo" data-slide="next">
        <span class="icon-keyboard_arrow_right"></span>
    </a>

</div>
@endif
<section class="shipping-info">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-6">
                <div class="row">
                     <div class="col-sm-4">
                          <img src="imgs/family1.jpg">
                     </div>

                     <div class="col-sm-8">
                         <div class="freewship">
                             <b>FAMILY BUSINESS</b>
                         </div>

                         <div class="fship">
                             By The Family, For the Families
                         </div>
                     </div>
                </div>
            </div>

            <div class="col-xl-3 col-6">
                <div class="row">
                     <div class="col-sm-4">
                          <img src="imgs/free-shipping.jpg">
                     </div>

                     <div class="col-sm-8">
                         <div class="freewship">
                             <b>FREE SHIPPING</b>
                         </div>

                         <div class="fship">
                             Free Shipping Worldwide On All Orders
                         </div>
                     </div>
                </div>
            </div>

            <div class="col-xl-3 col-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="imgs/cyber-security.jpg">
                    </div>

                    <div class="col-sm-8">
                        <div class="freewship">
                            <b>SECURE SHOPPING</b>
                        </div>

                        <div class="fship">
                            <div>World’s Most Secure Payment Methods</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="imgs/eco-friendly.jpg">
                    </div>

                    <div class="col-sm-8">
                        <div class="freewship">
                            <b>ECO FRIENDLY</b>
                        </div>

                        <div class="fship">
                            <div>100% Natural And Eco Friendly</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="browse-category">
    <div class="category-head">
        <i class="icon-keyboard_arrow_left"></i>Browse by Category<i class="icon-keyboard_arrow_right"></i>
    </div>
     <div class="container">
        <div class="category-width">
            <div class="row">
                @foreach($categories as $cat)
                    <div class="col-lg-3 col-6">
                        <a href="{{ url($cat->mslug.'/?category='.$cat->category_slug) }}" style="color: #333333">
                            <div>
                                <img src="{{ url('imgs/category/'.$cat->category_image) }}" class="gift">
                                <div class="img-wtext">
                                    <div>{{ $cat->category_name }}</div>
                                    <!-- <i>{{ $cat->product_count }} products</i> -->
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
     </div>
</section>

<section class="toys-collection">
    <div class="toys-head">
        OUR PICKS
        <a href="" class="see-all">
            see all
        </a>
    </div>

     <div class="container">
        <div class="row toy-hub">
            @foreach($products as $pro)
                <div class="col-xl-3 col-6">
                @include('frontend.common.productpart')
                </div>
            @endforeach
        </div>
     </div>
</section>

<section class="blog">
    <div class="blog-head">
        Blog
    </div>
    <div class="container">

        <div class="owl-carousel owl-carousel2 owl-theme">
	        @foreach($blogs as $blog)
            <div class="item">
                @include('frontend.common.blogpart')
            </div>
			@endforeach
        </div>

        <div class="see">
            <a href="{{ url('blog') }}">
                See more <i class="icon-keyboard_arrow_right"></i>
            </a>
        </div>
    </div>
</section>
