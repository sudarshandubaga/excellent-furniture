@php
$redirect_url = app('request')->input('redirect_url');
@endphp
<section class="login-part">
     <div class="container">
         <div class="login-info">
              <form method="post" data-redirect="{{ $redirect_url }}" id="loginForm" action="{{ url('ajax/user_login') }}">
                  @csrf
                  @if (\Session::has('success'))
                      <div class="alert alert-success">
                          {!! \Session::get('success') !!}</li>
                      </div>
                  @endif
                  <div class="form-group">
                      <label>
                          Email ID / Username
                      </label>

                      <input type="text" name="record[user_login]" class="form-control field-shadow" placeholder="Email ID / Username" required>
                  </div>

                  <div class="form-group">
                      <label>
                          Password
                      </label>

                      <input type="password" name="record[user_password]" class="form-control field-shadow" placeholder="Password" autocomplete="new-password" required>
                  </div>

                  <div class="newuser-link">
                      If you are a new user, then »
                      <a href="{{ url('register') }}" class="sign-anker">
                          Sign Up Here
                      </a>
                  </div>

                  <div class="form-group">
                      <button type="submit" class="form-control sign-in">
                          Sign In &nbsp;<i class="icon-long-arrow-right"></i>
                      </button>
                  </div>
              </form>
         </div>
     </div>
</section>
