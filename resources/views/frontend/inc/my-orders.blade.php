<section class="profile-block wishlist">
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                @include('frontend.common.profile_side')

                <div class="col-xl-8">
                    <div class="Change_Pass">
                        <h3>Your Orders</h3>
                        <hr>
                        @if(!$records->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Order ID</th>
                                        <th>Subtotal</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                        <th>Order Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sn = 0; @endphp
                                    @foreach($records as $rec)
                                    <tr>
                                        <td>{{ ++$sn }}</td>
                                        <td>
                                            <a href="{{ url('order/'.$rec->order_id) }}">{{ sprintf("#CHT%06d", $rec->order_id) }}</a>
                                        </td>
                                        <td>{{ $rec->order_currency == 'INR' ? '₹' : '$' }} {{ $rec->order_total }}</td>
                                        <td>{{ $rec->order_currency == 'INR' ? '₹' : '$' }} {{ $rec->order_discount }}</td>
                                        <td>{{ $rec->order_currency == 'INR' ? '₹' : '$' }} {{ $rec->order_total - $rec->order_discount }}</td>
                                        <td>{{ date("M d, Y", strtotime($rec->order_created_on)) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="bg-danger text-white p-2 text-center">
                                <i class="icon-thumb_down_alt mr-2"></i> No record(s) found.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
