<section class="profile-block wishlist">
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                @include('frontend.common.profile_side')

                <div class="col-xl-8">
                    <div style="overflow-x: unset;" class="table-responsive">
        				<table class="table table-bordered table-striped ">
        					<thead>
        						<tr>
        							<th colspan="2">Order Info</th>
        						</tr>
        					</thead>
        					<tbody>
        						<tr>
        							<td>
        								<div class="row">
        									<div class="col-sm-4">Order ID<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">#{{ sprintf('%06d', $record->order_id) }}</div>
        								</div>
        								<div class="row">
        									<div class="col-sm-4">Order Date<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ date('d / m / Y', strtotime($record->order_created_on)) }}</div>
        								</div>
        								<div class="row">
        									<div class="col-sm-4">Order Status<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ $record->order_status }}</div>
        								</div>
        							</td>
        							<td>
        								<div class="row">
        									<div class="col-sm-4">Payment Status<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ $record->order_is_paid }}</div>
        								</div>
        								<div class="row">
        									<div class="col-sm-4">Txn. ID<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ !empty( $record->order_txn_id ) ? $record->order_txn_id : "-" }}</div>
        								</div>
        								<div class="row">
        									<div class="col-sm-4">Paid Via<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">@if($record->order_is_paid == 'Y') {{ $record->order_currency == 'INR' ? 'Razorpay' : '2checkout Pay' }} @else &mdash; @endif</div>
        								</div>
        							</td>
        						</tr>
        					</tbody>
        					<thead>
        						<tr>
        							<th style="width: 50%;">Billing Address</th>
        							<th style="width: 50%;">Shipping Address</th>
        						</tr>
        					</thead>
        					<tbody>
        						@php
        							$sn = 0;
        							$billing 	= unserialize( html_entity_decode( $record->order_billing ) );
        							$shipping 	= unserialize( html_entity_decode( $record->order_shipping ) );
        						@endphp
        						<tr>
        							<td>
        								<div class="row">
        									<div class="col-sm-4">Name<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['name'] }}</div>

        									<div class="col-sm-4">Mobile No.<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['phone'] }}</div>

        									<div class="col-sm-4">Email.<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['email'] }}</div>

        									<div class="col-sm-4">Address 1<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['address1'] }}</div>

        									<div class="col-sm-4">Address 2<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['address2'] }}</div>

        									<div class="col-sm-4">City<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['city'] }}</div>

        									<div class="col-sm-4">State<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['state'] }}</div>

        									<div class="col-sm-4">Country<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['country'] }}</div>

        									<div class="col-sm-4">Postcode<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$billing['postcode'] }}</div>
        								</div>
        							</td>
        							<td>
        								<div class="row">
        									<div class="col-sm-4">Name<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['name'] }}</div>

        									<div class="col-sm-4">Mobile No.<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['phone'] }}</div>

        									<div class="col-sm-4">Email.<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['email'] }}</div>

        									<div class="col-sm-4">Address 1<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['address1'] }}</div>

        									<div class="col-sm-4">Address 2<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['address2'] }}</div>

        									<div class="col-sm-4">City<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['city'] }}</div>

        									<div class="col-sm-4">State<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['state'] }}</div>

        									<div class="col-sm-4">Country<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['country'] }}</div>

        									<div class="col-sm-4">Postcode<span class="float-right">:</span></div>
        									<div class="col-sm-8 overflow">{{ @$shipping['postcode'] }}</div>
        								</div>
        							</td>
        						</tr>
        					</tbody>
        					<thead>
        						<tr>
        							<th colspan="2">Customer Notes</th>
        						</tr>
        					</thead>
        					<tbody>
        						<tr>
        							<td colspan="2">
        								{!! $record->order_message !!}
        							</td>
        						</tr>
        					</tbody>
        				</table>
        				@if(!$ship->isEmpty())
        				<h5 class="float-left mt-3">Products</h5>
        				<table class="table table-bordered table-striped text-center">
        					<thead>
        						<tr>
        							<th>S.No.</th>
        							<th>Image</th>
        							<th>Name / Code</th>
        							<th>Rate</th>
        							<th>Qty</th>
        							<th>Subtotal</th>
        							<th>
        								Discount
        								@if(!empty($record->order_coupon))
        								<br>
        								<small>{{ $record->order_coupon }} ( {{ $record->coupon_discount }}% )</small>
        								@endif
        							</th>
        							<th>Total</th>
        						</tr>
        					</thead>
        					<tbody>
        						@php  $sn = $grandtotal = $totSubtotal = $totDiscount = 0; @endphp
        						@foreach($ship as $sh)
        							@php
        								$sn++;
        								$rate   	= $sh->opro_price;
        								$total 		= $rate * $sh->opro_qty;
        								$discount 	= !empty($record->coupon_discount) ? round($total * $record->coupon_discount / 100, 2) : 0;
        								$total 	   -= $discount;

        								$totSubtotal    += $rate * $sh->opro_qty;
        								$grandtotal 	+= $total;
        								$totDiscount 	+= $discount;
        							@endphp
        						<tr>
        							<td>{{ $sn }}</td>
        							<td>
        								<a href="{{ url('product-single/'.$sh->product_slug) }}">
        									<img style="width:60px;" src="{{ url('imgs/product/'.$sh->product_image) }}">
        								</a>
        							</td>
        							<td>
        								<a style="color:black;" href="{{ url('product-single/'.$sh->product_slug) }}">
        									{{ $sh->product_name }} / {{ $sh->product_code }}
        								</a>
        							</td>
        							<td>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $rate }}</td>
        							<td>{{ $sh->opro_qty }}</td>
        							<td>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $rate * $sh->opro_qty }}</td>
        							<td>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $discount }}</td>
        							<td>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $total }}</td>
        						</tr>
        						@endforeach
        					</tbody>
        					<tfoot>
        						<tr>
        							<th colspan="5">TOTAL</th>
        							<th>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $totSubtotal }}</th>
        							<th>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $totDiscount }}</th>
        							<th>{{ $record->order_currency == 'INR' ? '₹' : '$' }} {{ $grandtotal }}</th>
        						</tr>
        					</tfoot>
        				</table>
        				@endif
        			</div>
                </div>
            </div>
        </form>
    </div>
</section>
