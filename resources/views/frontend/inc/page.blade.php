<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mt-3">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $query->page_title }}</li>
        </ol>
    </nav>

    <div class="shipping-head">
        {{ $query->page_title }}
    </div>

    <div>
        {!! $query->page_description !!}
    </div>
</div>
