@php
     // Shortens a number and attaches K, M, B, etc. accordingly
     function number_shorten($number, $precision = 3, $divisors = null) {

     // Setup default $divisors if not provided
     if (!isset($divisors)) {
        $divisors = array(
            pow(1000, 0) => '', // 1000^0 == 1
            pow(1000, 1) => 'K', // Thousand
            pow(1000, 2) => 'M', // Million
            pow(1000, 3) => 'B', // Billion
            pow(1000, 4) => 'T', // Trillion
            pow(1000, 5) => 'Qa', // Quadrillion
            pow(1000, 6) => 'Qi', // Quintillion
        );
     }

     // Loop through each $divisor and find the
     // lowest amount that matches
     foreach ($divisors as $divisor => $shorthand) {
        if (abs($number) < ($divisor * 1000)) {
            // We found a match!
            break;
        }
     }

     // We found our match, or there were no matches.
     // Either way, use the last defined value for $divisor.
     return number_format($number / $divisor, $precision) . $shorthand;
     }
     $cart_session = session('cart_session');
@endphp
<section class="product-whole-information">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url($record->mslug) }}">{{ ucwords(strtolower($record->mcategory)) }}</a></li>
                <!-- <li class="breadcrumb-item"><a href="{{ url($record->sslug) }}">{{ $record->scategory }}</a></li> -->
                <li class="breadcrumb-item active" aria-current="page">{{ $record->product_name }}</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-xl-6">
                <div class="xzoom-container">
                    <div id="demo2" class="carousel slide" data-ride="carousel">
                      <!-- The slideshow -->
                      <div class="carousel-inner">
                          <a class="lightbox carousel-item active" data-fancybox="gallery" href="{{ url('imgs/product/'
                          .$record->product_image) }}">
                              <img class="xzoom x-img" id="xzoom-default" src="{{ url('imgs/product/'
                              .$record->product_image) }}" />
                          </a>
                          @foreach($gall as $images)
                          <a class="lightbox carousel-item" data-fancybox="gallery" href="{{ url('imgs/product/'.$record->product_id.'/'.$images->pimage_image) }}" data-fancybox="gallery">
                              <img class="xzoom x-img" src="{{ url('imgs/product/'.$record->product_id.'/'.$images->pimage_image) }}">
                          </a>
                          @endforeach
                      </div>
                        <!-- Indicators -->
                        <div class="xzoom-thumbs carousel-indicators clearfix owl-carousel owl-carousel3 owl-theme">
                          <div data-target="#demo2" data-slide-to="0" class="active item"><img
                                      class="xzoom-gallery" src="{{ url('imgs/product/'.$record->product_image_thumb) }}"  xpreview="{{ url
                                      ('imgs/product/'.$record->product_image_thumb) }}" title="">
                          </div>
                          @php $i = 0; @endphp
                          @foreach($gall as $images)
                               @php $i++; @endphp
                          <div data-target="#demo2" data-slide-to="{{ $i }}" class="item"><img
                                     class="xzoom-gallery" src="{{ url('imgs/product/'.$record->product_id.'/'.$images->pimage_image_thumb) }}"  xpreview="{{ url
                                     ('imgs/product/'.$record->product_id.'/'.$images->pimage_image_thumb) }}" title="">
                          </div>
                          @endforeach
                      </div>

                    </div>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="product-name">
                    {{ $record->product_name }}
                </div>

                <div class="star-twelve">
                    @php
                        $session_auth = session('user_auth');
                        $exist = DB::table('wishlist')->where('wish_uid', $session_auth)->where('wish_pid', $record->product_id)->first();
                        $avgrating = DB::table('reviews')->where('review_pid', $record->product_id)->select(DB::raw('AVG(review_rating) AS avg'))->first();

                        $ratingcount = DB::table('reviews')->where('review_pid', $record->product_id)->count();
                    @endphp
                    @if(!empty($ratingcount))
                    <span style="color: #4d4d4d;">
                        @for($i = 1; $i <= $avgrating->avg; $i++)
                        <i class="icon-star"></i>
                        @endfor
                        @php
                        $arr = explode(".", $avgrating->avg);
                        @endphp
                        @if(!empty($arr[1] != 0))
                        @php $i++; @endphp
                        <i class="icon-star_half"></i>
                        @endif
                        @for($j = $i; $j <= 5; $j++)
                         <i class="icon-star_outline"></i>
                        @endfor
                    </span>
                    <span style="color: #6b6b6b;">{{ $ratingcount }} Reviews</span>
                    @endif
                </div>

                <div class="product-head-two">
                    <b class="product-price" data-usd="{{ $record->product_sell_price_dollar }}" data-inr="{{ $record->product_sell_price }}">{{ $country_code == 'IN' ? '₹ '.$record->product_sell_price : '$ '.$record->product_sell_price_dollar }} </b>
                    @if($record->product_sell_price < $record->product_original_price && $country_code == 'IN')&nbsp;
                    &nbsp;<strike class="product-price" data-usd="{{ $record->product_original_price_dollar }}" data-inr="{{ $record->product_original_price }}">₹ {{$record->product_original_price }}</strike>
                    @endif
                    @if($record->product_sell_price_dollar < $record->product_original_price_dollar && $country_code != 'IN')&nbsp;
                    &nbsp;$ <strike class="product-price" data-usd="{{ $record->product_original_price_dollar }}" data-inr="{{ $record->product_original_price }}">{{$record->product_original_price_dollar }}</strike>
                    @endif
                </div>

                @if($record->product_in_stock == 'Y')
                <div class="row">
                    <div class="col-sm-5 col-6">
                        <div class="qty-product text-center">
                            <label class="qty_title">Quantity</label>
                            <div class="increment input-group">
                                <div class="input-group-prepend">
                                  <button class="add minus update_cart_value" type="button">-</button>
                                </div>
                                <input type="text" data-pid="{{ $record->product_id }}" data-action="{{ url('ajax/update') }}" value="@if(!empty($cart_session[$record->product_id])){{ $cart_session[$record->product_id] }} @else 1 @endif" name="noOfRoom" class="text-center txt_wth qty_input custom num-filled form-control" id="productQty" />
                                <div class="input-group-append">
                                  <button class="odd plus update_cart_value" type="button">+</button>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 col-6">
                        <button type="button" style="background:transparent;" class="atc add_to_cart_product" data-url="{{ url('ajax/add-to-cart')
                   }}" data-id="{{ $record->product_id }}">ADD TO CART</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="two-atc-click">
                            <a class="atc-two wishlist text-white d-inline-block text-center" data-id="{{ $record->product_id }}" data-url="{{
                           url('ajax/wishlist')}}">
                               add to wishlist
                            </a><span class="wish-count d-inline-block">{{ number_shorten($wishcount, 2) }}</span>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="two-atc-click">
                            <button class="buy-nowbtn" data-url="{{ url('ajax/add-to-cart')
                       }}" data-id="{{ $record->product_id }}">Buy Now</button>
                        </div>
                    </div>
                </div>
                @else
                    <div class="mb-3">OUT OF STOCK</div>
                    <form id="notifyForm" action="{{ url('ajax/notify_me') }}" method="post">
                        <div class="">
                            Notify me when its in stock.
                        </div>
                        <input type="hidden" name="product_slug" value="{{ $record->product_slug }}">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <input type="text" name="email" placeholder="Email Address" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Send</button>
                                </div>
                            </div>
                        </div>
                    </form>
                @endif

                <div class="toys-desc">
                    {!! $record->product_description !!}
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
         <div class="review-boundary">
             <div class="row">
                 <div class="col-xl-8">
                     <div class="cr-head">
                         Customer Reviews
                         @php
                              $avgrating = DB::table('reviews')->where('review_pid', $record->product_id)->select(DB::raw('AVG(review_rating) AS avg'))->first();
                         @endphp
                     </div>

                     @if (\Session::has('success'))
                        <div class="alert alert-success">
                            {!! \Session::get('success') !!}</li>
                        </div>
                    @endif
                     <div class="row">
                         <div class="col-xl-4">
                             <div>
                                  <span style="color: #4d4d4d;">
                                        <!-- <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i>
                                        <i class="icon-star"></i> -->

                                        @for($i = 1; $i <= $avgrating->avg; $i++)
                                        <i class="icon-star"></i>
                                        @endfor
                                        @php
                                        $arr = explode(".", $avgrating->avg);
                                        $total_count = DB::table('reviews')->where('review_pid', $record->product_id)->count();
                                        @endphp
                                        @if(!empty($arr[1]))
                                        @php $i++; @endphp
                                        <i class="icon-star_half"></i>
                                        @endif
                                        @for($j = $i; $j <= 5; $j++)
                                         <i class="icon-star_outline"></i>
                                        @endfor
                                  </span>&nbsp;&nbsp;
                             </div>

                             <div class="bor">
                                 Based on {{ $total_count }} reviews
                             </div>
                         </div>

                         <div class="col-xl-8">
                             @for($i=5;$i>=1;$i--)
                             @php

                              $ratingcount = DB::table('reviews')->where('review_pid', $record->product_id)->where('review_rating', $i)->count();
                                   @$per = ($ratingcount / $total_count) * 100;
                              @endphp
                                 <div>
                                     <span style="color: #4d4d4d;">
                                          @for($k = 1; $k <= $i; $k++)
                                         <i class="icon-star"></i>
                                         @endfor
                                         @for($j = 1; $j <= 5-$i; $j++)
                                          <i class="icon-star_outline"></i>
                                         @endfor
                                     </span>&nbsp;&nbsp;

                                     <span class="red-line">
                                          <!-- Green -->
                                          <div class="progress">
                                            <div class="progress-bar" style="width:{{ $per }}%; background:#78e5d0;"></div>
                                          </div>
                                     </span>&nbsp;

                                     <span>{{ $per }}%</span>&nbsp;

                                     <span>({{ $ratingcount }})</span>
                                 </div>
                             @endfor
                         </div>
                     </div>
                 </div>

                 <div class="col-xl-4">
                     <div class="text-right" style="margin-top: 40px;">
                         <a @if(!empty($user)) href="" data-toggle="modal" data-target="#myModal" @else href="{{ url('user') }}" @endif  class="war-two">
                             Write a review
                         </a>
                         <form method="post">
                              @csrf
                              <div id="myModal" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                      <!-- Modal content-->
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <input type="hidden" name="rating[review_pid]" value="{{ $record->product_id }}">
                                              <input type="hidden" name="rating[review_uid]" value="{{ $user }}">
                                              <input name="rating[review_rating]" type="text"
                                                     class="rating rating-loading" value="0"
                                                     data-size="xs" data-step="1" title="" />
                                          </div>

                                          <div class="modal-body">
                                              <textarea class="form-control" rows="8" cols="20"
                                                        name="rating[review_comment]" placeholder=" Please write a review"></textarea>
                                          </div>

                                          <div class="modal-footer">
                                              <button type="submit" class="form-control add-comment">
                                                  Add Comment
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                         </form>
                     </div>

                     <!-- <div class="float-right">
                         <select class="form-control drop-down">
                             <option>Most Recent</option>
                             <option></option>
                             <option></option>
                             <option></option>
                         </select>
                     </div> -->
                 </div>
             </div>

             <div class="container">
                 @foreach($reviews as $rev)
                 @php
                 $rating = round($rev->review_rating);
                 $likes = DB::table('likes')
                         ->where('like_rid', $rev->review_id)
                         ->select(DB::raw('SUM(`like_like`) AS lk_total, SUM(`like_dislike`) AS dlk_total'))
                         ->first();
                 $isExixts = DB::table('reviews as re')
                                ->join('likes as li', 'li.like_rid', 're.review_id')
                                ->where('re.review_uid', $user)
                                ->where('re.review_pid', $record->product_id)
                                ->where('re.review_id', $rev->review_id)
                                ->get();
                            //print_r($isExixts);

                 @endphp
                 <div class="upper-lower">
                     <div class="clearfix">
                         <div class="circle review_image float-left">
                              <img width="100%" src="{{ url('imgs/images.png') }}" alt="">
                              <i class="icon-check_circle"></i>
                         </div>

                         <div class="float-left">
                             <div class="">
                                 <span style="color: #4d4d4d;"class="verified-star">
                                     <!-- <i class="icon-star"></i>
                                     <i class="icon-star"></i>
                                     <i class="icon-star"></i>
                                     <i class="icon-star"></i>
                                     <i class="icon-star"></i> -->
                                     @for($i = 1; $i <= $rating; $i++)
                                     <i class="icon-star"></i>
                                     @endfor
                                     @for($j = 1; $j <= 5-$rating; $j++)
                                      <i class="icon-star_outline"></i>
                                     @endfor
                                 </span>&nbsp;&nbsp;
                                 <span class="verifi-date">{{ date("d M Y", strtotime($rev->review_created_on)) }}</span>
                             </div>

                             <div class="">
                                 <span class="verifi-block">Verified</span>

                                 <span class="name-block">{{ $rev->user_name }}</span>
                             </div>
                         </div>
                     </div>

                     <!-- <div>
                         <b>
                             Lovee!
                         </b>
                     </div> -->

                     <div>{{ $rev->review_comment }}</div>

                     <div class="text-right">
                         <i class="icon-thumb_up_alt like " data-rid="{{ $rev->review_id }}" data-like="1" data-dislike="0" data-url="{{
                        url('ajax/like')}}"></i> &nbsp;<span class="vallike">{{ $likes->lk_total }}</span>&nbsp;
                         <i class="icon-thumb_down_alt like" data-rid="{{ $rev->review_id }}" data-like="0" data-dislike="1" data-url="{{
                        url('ajax/like')}}"></i> &nbsp;<span class="valdislike">{{ $likes->dlk_total }}</span>&nbsp;
                     </div>
                 </div>
                 @endforeach

                 <div class="text-center">
                    <ul class="page-no">
                        {{ $reviews->links() }}
                    </ul>
                 </div>
             </div>
         </div>
    </div>
</section>

<section class="releated-product">
    <div class="releated-head text-center">
        releated products
    </div>
    <div class="container">
        <div class="owl-carousel owl-theme">
            @foreach($related as $pro)
            <div class="item">
                <div >
                 @include('frontend.common.productpart')
               </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<div class="recently-head recently">
    <div class="container">
        you recently viewed
        <!-- <span class="float-right">
            <a href="{{ url('product/'.$slug.'/clear') }}" class="crv">
                clear recently viewed
            </a>
        </span> -->
    </div>
</div>
<div class="container">
    <section class="recently-viewed">

        <div>
                <div class="owl-carousel owl-theme">
                    @foreach($recents as $pro)
                    <div class="item">
                        <div >
                         @include('frontend.common.productpart')
                       </div>
                    </div>
                    @endforeach
                </div>
        </div>
    </section>
</div>
