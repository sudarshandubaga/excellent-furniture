@php
    if(!empty($post['category'])) {
        $mcategory  = $post['category'];
        $category   = DB::table('categories')->where('category_slug', $mcategory)->first();
        $mcat_name  = $category->category_name;

        $subcategories = DB::table('categories')->join('categories AS mcat', 'categories.category_parent', '=',  'mcat.category_id')
            ->where('mcat.category_slug', $mcategory)
            ->where('categories.category_is_deleted', 'N')
            ->select('categories.*')
            ->get();
    } else if(!isset($subcategories)) {
        $subcategories = [];
    }

    $query = DB::table('products')->join('categories AS cat', 'products.product_category', '=', 'cat.category_id')
                ->leftJoin('categories AS scat', 'products.product_subcategory', '=', 'scat.category_id')
                ->where('product_is_deleted', 'N')
                ->select('products.*', 'cat.category_name AS mcat', 'scat.category_name AS scat');

    if(!empty($mcategory)) {
        $query->where('cat.category_slug', $mcategory);
    }

    $searchBar = ''; $url_sufix = '';
    if(!empty($post['subcategory'])) {
        $s = $post['subcategory'];

        $query->where('scat.category_slug', $s);

        $searchBar .= '<a href="#category_'.$s.'" class="radius-text"> '.$s.'&nbsp;&nbsp;&nbsp;<i class="icon-cross"></i></a>';
        $url_sufix  = '/?category='.urlencode($s);
    } elseif(!empty($scategory)) {
        $query->where('scat.category_slug', $scategory);
    }

    if(!empty($post['s'])) {
        $query->where('product_name', 'LIKE', '%'.$post['s'].'%');
    }


    if(!empty($post['sort_by'])) {
        $sort_by = $post['sort_by'];

        switch($sort_by) {
            case 'Price: Low to High':
                $query->orderBy('product_sell_price');
                break;
            case 'Price: High to Low':
                $query->orderBy('product_sell_price', 'DESC');
                break;
            case 'Best Seller':
                $query->orderBy('product_order');
                break;
            case 'Ratings':
                $query->orderByRaw('(SELECT AVG(review_rating) FROM `ec_reviews` WHERE `review_pid` = ec_products.product_id) DESC');
                break;
        }

        $url_sufix  .= !empty($url_sufix) ? '&sort_by='.urlencode($sort_by) : '/?sort_by='.urlencode($sort_by);
    }

    /* if(!empty($post['search'])) {
        $s = $post['search'];

        if(!empty($s['colors'])) {
            $colors = $s['colors'];
            $query->whereNested(function($q) use ($colors, $searchBar) {
                $q->whereRaw('1=2');
                foreach($colors as $col) {
                    $q->orWhereRaw('FIND_IN_SET(?, product_color)', [$col]);
                }
            });

            foreach($colors as $col) {
                $color = DB::table('colors')->where('color_id', $col)->first();

                $searchBar .= '<a href="#color_'.$color->color_id.'" class="radius-text"> '.$color->color_name.'&nbsp;&nbsp;&nbsp;<i class="icon-cross"></i></a>';
            }
        }

        if(!empty($s['age'])) {
            $ages = $s['age'];
            $query->whereNested(function($q) use ($ages) {
                $q->whereRaw('1=2');
                foreach($ages as $age) {
                    $q->orWhereRaw('FIND_IN_SET(?, product_age)', [$age]);
                }
            });

            foreach($ages as $age) {

                $searchBar .= '<a href="#age_'.$age.'" class="radius-text"> '.$age.'&nbsp;&nbsp;&nbsp;<i class="icon-cross"></i></a>';
            }

        }
    } */

    // echo $query->toSql();

    $products = $query->get();
@endphp

<div class="sorting-strip">
    <form action="{{ url('ajax/search_products') }}" id="sortingForm" method="post" style="margin: 0">
        <div class="row">
            <div class="col-sm-2 col-5">
                {{ $products->count() }} product(s)
            </div>
            <div class="col-sm-8 col-2 order-3 order-md-2">
                <ul class="subcategory-menu">
                    <li><a href="#" data-subcategory="" rel="product-filter" data-category="{{ @$mcategory }}">All Products</a></li>
                    @foreach($subcategories as $sc)
                    <li>
                        <i class="icon-star"></i>
                        <a href="{{ url('toys?category='.$sc->category_slug) }}" data-subcategory="{{ $sc->category_slug }}" data-category="{{ @$mcategory }}" rel="product-filter">{{ $sc->category_name }}</a>
                    </li>
                    @endforeach
                </ul>
                <div class="text-right filter-icon d-block d-sm-none" style="font-size: 22px;">
                    <span class="icon-filter3"></span>
                </div>
                <input type="hidden" name="category" value="{{ @$mcategory }}" id="sorting_category">
                <input type="hidden" name="subcategory" value="{{ @$s }}" id="sorting_subcategory">
            </div>
            <div class="col-sm-2 col-5 text-right order-2 order-md-3">
                <select name="sort_by" class="form-control sort_by_dropdown" data-category="{{ @$mcategory }}">
                    <option value="">Sort By</option>
                    <option value="Price: Low to High" @if(@$sort_by == "Price: Low to High") selected @endif>Price: Low to High</option>
                    <option value="Price: High to Low" @if(@$sort_by == "Price: High to Low") selected @endif>Price: High to Low</option>
                    <option value="Best Seller" @if(@$sort_by == "Best Seller") selected @endif>Best Seller</option>
                    <option value="Ratings" @if(@$sort_by == "Ratings") selected @endif>Ratings</option>
                </select>
            </div>
        </div>
    </form>
</div>

@if(!$products->isEmpty())
    <div class="product-images">
        <div class="row side-free">
            @foreach($products as $pro)
            <div class="col-xl-3 col-md-4 col-6">
                @include('frontend.common.productpart')
            </div>
            @endforeach
        </div>
        <div class="product_loading">
            <div class="loading-bg"> </div>
            <div class="loading-body text-center">
                <div class="spinner-border mb-2" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <h3 style="font-size: 2em">Loading</h3>
            </div>
        </div>
    </div>

@else
    <div class="no_records_found">
        <div class="no_records_icon">
            <i class="icon-sad"></i>
        </div>
        Oops! No record(s) found.
    </div>
@endif
