<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mt-3">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $mcat_name }}</li>
        </ol>
    </nav>
    <section class="options">
        <div class="row">
            <div class="col-xl-12">
                <div id="productGrid">
                    @include('frontend.inc.product_grid')
                </div>
            </div>
        </div>
    </section>
</div>
