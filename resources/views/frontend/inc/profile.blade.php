<section class="profile-block">
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">
            @include('frontend.common.profile_side')

            <div class="col-xl-8">
                 <div class="profile-head">
                     Profile
                 </div>
                 <hr>
                 @if (\Session::has('success'))
                     <div class="alert alert-success">
                         {!! \Session::get('success') !!}</li>
                     </div>
                 @endif
                 @if (\Session::has('failed'))
                     <div class="alert alert-danger">
                         {!! \Session::get('failed') !!}</li>
                     </div>
                 @endif
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control field-shadow" placeholder="First Name" name="record[user_fname]" value="{{ $record->user_fname }}" required>
                    </div>

                    <div class="col-sm-6 form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control field-shadow" name="record[user_lname]" value="{{ $record->user_lname }}" placeholder="Last Name" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Email</label>
                        <input type="text" class="form-control field-shadow" name="record[user_email]" value="{{ $record->user_email }}" placeholder="Email" required>
                    </div>

                    <div class="col-sm-6 form-group">
                        <label>Mobile</label>
                        <input type="tel" class="form-control field-shadow" name="record[user_mobile]" value="{{ $record->user_mobile }}" placeholder="Mobile No." required>
                    </div>
                </div>

                <h3>Address</h3>
                <hr>

                <div class=" form-group">
                    <label>Address Line1</label>
                    <input type="text" class="form-control field-shadow" name="record[user_address1]" value="{{ @$record->user_address1 }}" placeholder="Address">
                </div>

                <div class=" form-group">
                    <label>Address Line2</label>
                    <input type="text" class="form-control field-shadow" name="record[user_address2]" value="{{ @$record->user_address2 }}" placeholder="Address">
                </div>
                <div class="row">

                    <div class="col-sm-6 form-group">
                        <label>Country</label>
                        <select class="form-control" name="record[user_country]">
                            @foreach($countries as $con)
                            <option value="{{ $con->country_code }}" @if($con->country_code == @$record->user_country) selected @endif>{{ $con->country_name.' ('.$con->country_code.')' }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-6 form-group">
                        <label>State</label>
                        <input type="text" class="form-control field-shadow" name="record[user_state]" value="{{ @$record->user_state }}" placeholder="State/County">
                    </div>

                    <div class="col-sm-6 form-group">
                        <label>City</label>
                        <input type="text" class="form-control field-shadow" name="record[user_city]" value="{{ @$record->user_city }}" placeholder="State/County">
                    </div>

                    <div class="col-sm-6 form-group">
                        <label>Pincode / Zipcode</label>
                        <input type="text" class="form-control field-shadow" name="record[user_pincode]" value="{{ @$record->user_pincode }}" placeholder="Pincode / Zipcode">
                    </div>
                </div>

                <div>
                    <button type="submit" class="save-button form-control">
                         SAVE
                    </button>
                </div>
            </div>
        </div>
    </form>
    </div>
</section>
