<section class="register-part">
    <div class="container">
         <div class="register-info">
             <form method="post">
                 @csrf
                 @if (\Session::has('success'))
                     <div class="alert alert-success">
                         {!! \Session::get('success') !!}</li>
                     </div>
                 @endif

                 @if (\Session::has('danger'))
                     <div class="alert alert-danger">
                         {!! \Session::get('danger') !!}</li>
                     </div>
                 @endif
                 <div class="row">
                     <div class="col-sm-6 form-group">
                         <label>First Name</label>
                         <input type="text" name="record[user_fname]" class="form-control field-shadow" placeholder="First Name" required>
                     </div>

                     <div class="col-sm-6 form-group">
                         <label>Last Name</label>
                         <input type="text" name="record[user_lname]" class="form-control field-shadow" placeholder="Last Name" required>
                     </div>
                 </div>

                 <div class="row">
                     <div class="col-sm-6 form-group">
                         <label>Email</label>
                         <input type="text" name="record[user_email]" class="form-control field-shadow" placeholder="Email" required>
                     </div>

                     <div class="col-sm-6 form-group">
                         <label>Mobile</label>
                         <input type="tel" name="record[user_mobile]" class="form-control field-shadow" placeholder="Mobile No." required>
                     </div>
                 </div>

                 <div class="row">
                     <div class="col-sm-6 form-group">
                         <label>Password</label>
                         <input type="password" name="record[user_password]" class="form-control field-shadow" placeholder="Password" autocomplete="new-password" required>
                     </div>

                     <div class="col-sm-6 form-group">
                         <label>Confirm Password</label>
                         <input type="password" name="record[confirm_password]" class="form-control field-shadow" placeholder="Confirm-Password" autocomplete="new-password" required>
                     </div>
                 </div>

                 <div class="row">
                     <div class="col-sm-4">
                         <div class="form-group">
                             <img src="{{ url('captcha-code').'?v='.time() }}" alt="" class="captcha-image">
                             <i class="icon-refresh refresh-captcha"></i>
                         </div>
                     </div>
                     <div class="col-sm-8">
                         <div class="form-group">
                             <input type="text" name="captcha" value="" placeholder="Enter Captcha Code" class="form-control" required>
                         </div>
                     </div>
                 </div>

                 <div class="form-group">
                     <button type="submit" class="form-control sign-up">
                         Sign Up &nbsp;<i class="icon-long-arrow-right"></i>
                     </button>
                 </div>
             </form>
         </div>
    </div>
</section>
