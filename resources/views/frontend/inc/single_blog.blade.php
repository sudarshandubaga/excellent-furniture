<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}" title="Home">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('blog') }}" title="Blog">Blog</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ ucwords($blog->post_title) }}</li>
        </ol>
    </nav>

    <div class="blog-main-head">
         {{ $blog->post_title }}
    </div>

                   <section class="blog-top">
                       <div class="blog-border">
                           <div style="color: #cca6a6; text-align: center">
                               {{ date("F d, Y", strtotime($blog->post_created_on)) }}
                           </div>

                           <div class="blog-des">
                               {!! $blog->post_description !!}
                           </div>
                       </div>
                   </section>

</div>
