<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb mt-3">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Toys</li>
        </ol>
    </nav>

    <section class="options">
        <div class="row">
            <div class="col-xl-3">
                <div class="filter-head">
                    <h4><i class="icon-filter1"></i> FILTERS</h4>
                </div>
                <form id="searchProductForm" action="{{ url('ajax/search_products') }}" method="post">
                    @csrf

                    @if(!empty($subcategories) && !$subcategories->isEmpty())
                    <div class="catgry-head">
                        <b>CATEGORY</b>
                    </div>


                    @foreach($subcategories as $cname)
                        <label class="xbox">
                            <input type="checkbox" name="search[categories][]" value="{{ $cname->category_id }}" class="ajax_action">
                            <span class="checkmark"></span>
                            <span class="name">{{ $cname->category_name }}</span>
                        </label>
                    @endforeach

                    @endif

                    <div class="price-head">
                        <b>PRICE</b>
                    </div>

                    <!-- Range slider, low and high tracks, and selection: -->
                    <input id="product_price_range" name="search[pricerange]" type="text" data-slider-min="-70.72" data-slider-max="2250" data-slider-value="[-70.72, 2250]" data-slider-tooltip="always" /><br/>
                    <div class="row mt-3 mb-3">
                        <div class="col-sm-6">
                            <span class="slider_value_heading">MIN:<span class="slider_value">-70.72</span></span>
                        </div>
                        <div class="col-sm-6 text-right">
                            <span class="slider_value_heading">MAX:<span class="slider_value">2250</span></span>
                        </div>
                    </div>

                    @if(!empty($colors) && !$colors->isEmpty())
                    <div>
                        <div class="color-head">
                            <b>COLORS</b>
                        </div>

                        @foreach($colors as $col)
                        <label class="xbox">
                            <input type="checkbox" name="search[colors][]" value="{{ $col->color_id }}" class="ajax_action" id="color_{{ $col->color_id }}">
                            <span class="checkmark"></span>
                            <span class="name">{{ $col->color_name }}</span>
                        </label>
                        @endforeach
                    </div>
                    @endif


                    <div>
                        <div class="color-head">
                            <b>AGE</b>
                        </div>

                        <label class="xbox">
                            <input type="checkbox" name="search[age][]" value="0-1" class="ajax_action" id="age_0-1">
                            <span class="checkmark"></span>
                            <span class="name">0-1</span>
                        </label>

                        <label class="xbox">
                            <input type="checkbox" name="search[age][]" value="1-2" class="ajax_action" id="age_1-2">
                            <span class="checkmark"></span>
                            <span class="name">1-2</span>
                        </label>

                        <label class="xbox">
                            <input type="checkbox" name="search[age][]" value="2-3" class="ajax_action" id="age_2-3">
                            <span class="checkmark"></span>
                            <span class="name">2-3</span>
                        </label>

                        <label class="xbox">
                            <input type="checkbox" name="search[age][]" value="3-6" class="ajax_action" id="age_3-6">
                            <span class="checkmark"></span>
                            <span class="name">3-6</span>
                        </label>
                    </div>
                </form>
            </div>

            <div class="col-xl-9">
                <div id="productGrid">
                    @include('frontend.inc.product_grid')
                </div>
            </div>
        </div>
    </section>
</div>
