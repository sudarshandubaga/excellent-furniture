<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Wholesale</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-xl-6">
            <div class="wholsale-head">
                Wholesale Inquiry
            </div>

            <div class="whole-desc">
                We love our work. We imagine and develop products for our next generation, the
                joyful products which makes them more creative and thoughtful. We will be more
                than happy to give shape to your idea or you can contribute to our’s. Please don’t
                hesitate to contact us regarding wholesale inquiry if you would like to sell any
                of our product or you want us to manufacture and deliver your own designs.
            </div>

            <div class="whole-desc">
                We are India’s leading manufacturer and Wholesale supplier of wooden toys. We
                deliver our products across the globe. whether you are a small/startup business or
                well established one, we are capable enough to fulfill your requirements.
            </div>

            <div class="whole-desc">
                Please raise your question in the comment box or type ‘Catalog’ only for list of                  wholesale wooden toy and price list.
            </div>
        </div>

        <div class="col-xl-6">
            {{ Form::open(array('method' => 'post')) }}
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}</li>
                </div>
            @endif
            <div class="form-group">
                <input type="text" name="record[name]" class="form-control whole-shadow" placeholder="Name" required>
            </div>

            <div class="form-group">
                <select class="form-control" name="record[country]" required>
                    <option value="">Select Country</option>
                    @foreach($countries as $con)
                    <option value="{{ $con->country_code }}">{{ $con->country_name.' ('.$con->country_code.')' }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="text" name="record[store_name]" class="form-control whole-shadow" placeholder="Store Name" required>
            </div>

            <div class="form-group">
                <input type="email" name="record[email]" class="form-control whole-shadow" placeholder="Email" required>
            </div>

            <div class="form-group">
                <input type="tel" name="record[phone]" class="form-control whole-shadow" placeholder="Phone no" required>
            </div>

            <div class="form-group">
                <textarea name="record[message]" class="form-control whole-shadow" placeholder="Message" cols="4" rows="8" required></textarea>
            </div>

            <!-- <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6LcfPrEUAAAAAFI1qyx06ouICBFIw2P60qACcYeP"></div>
            </div> -->

            <div class="form-group text-right">
                <button type="submit" class="submit-btn">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
