<section class="profile-block wishlist">
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            @csrf
        <div class="row">
            @include('frontend.common.profile_side')

            <div class="col-xl-8">
                <div class="row">
                    @foreach($records as $pro)
                    <div class="col-xl-4 wish_pro pr-0">
                        @include('frontend.common.productpart')
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </form>
    </div>
</section>
